import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  RouterStateSnapshot,
} from '@angular/router';
import { Observable } from 'rxjs';
import { AuthenticationService } from '../services/utils/authentication.service';
import { UserRoles } from '../types/user-roles';
import {
  matchesAccountingPath,
  matchesAproverPath,
  matchesConfigPath,
  matchesReportPath,
} from '../validators/path.validator';

@Injectable({
  providedIn: 'root',
})
export class RoleGuard implements CanActivate {
  constructor(private authService: AuthenticationService) {}

  public canActivate(
    route: ActivatedRouteSnapshot,
    _: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    const userRole: UserRoles = this.authService.authenticatedUser.role;
    const rawPath: string = route.url.toString();

    // Admin tiene acceso a todo
    if (userRole === UserRoles.ADMINISTRADOR) return true;

    // Customer: acceso a los servicios del banco a clientes no empresariales
    if (userRole === UserRoles.CUSTOMER) {
      return !matchesAccountingPath(rawPath);
    }

    // RPL solamente no tiene acceso a Contabilidad
    if (userRole === UserRoles.REPRESENTANTE_LEGAL) {
      return !matchesAccountingPath(rawPath);
    }

    // Ingresador acceso a lo que no hace el aprobador
    if (userRole === UserRoles.INGRESADOR) {
      return !matchesAproverPath(rawPath) && !matchesReportPath(rawPath);
    }

    // Aprobador acceso a reportes y a cosas del aprobador xd.
    if (userRole === UserRoles.APROBADOR) {
      return (
        matchesAproverPath(rawPath) ||
        matchesReportPath(rawPath) ||
        !matchesConfigPath(rawPath)
      );
    }

    return false;
  }
}
