import { AuthenticationService } from 'src/app/services/utils/authentication.service';
import { UserRoles } from './../../types/user-roles';
import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { MainLayoutComponent } from '../../layouts/app.main.component';
import {
  matchesAproverPath,
  matchesConfigPath,
  matchesReportPath,
} from 'src/app/validators/path.validator';

@Component({
  selector: 'app-menu',
  template: `
    <div class="layout-menu-container">
      <ul class="layout-menu" role="menu" (keydown)="onKeydown($event)">
        <li
          app-menu
          class="layout-menuitem-category"
          *ngFor="let item of model; let i = index"
          [item]="item"
          [index]="i"
          [root]="true"
          role="none"
        >
          <div class="layout-menuitem-root-text" [attr.aria-label]="item.label">
            {{ item.label }}
          </div>
          <ul role="menu">
            <li
              app-menuitem
              *ngFor="let child of item.items"
              [item]="child"
              [index]="i"
              role="none"
            ></li>
          </ul>
        </li>
      </ul>
    </div>
  `,
})
export class AppMenuComponent implements OnInit {
  public model: MenuItem[] = [];
  private rolePermissions: Map<UserRoles, MenuItem[]>;

  private businessMgmtItems: MenuItem;
  private balanceItems: MenuItem;
  private accountingItems: MenuItem;
  private collectionItems: MenuItem;
  private paymentsItems: MenuItem;
  private customerServicesItems: MenuItem;

  constructor(
    public appMain: MainLayoutComponent,
    private authService: AuthenticationService
  ) {
    this.customerServicesItems = {
      label: 'Servicios',
      icon: 'pi pi-fw pi-sliders-v',
      items: [
        {
          label: 'Pago de Servicios',
          icon: 'pi pi-fw pi-dollar',
          routerLink: ['/pay-service'],
        },
        {
          label: 'Configurar pagos recurrentes',
          icon: 'pi pi-fw pi-calendar-plus',
          routerLink: ['/config-recurrent-payments'],
        },
        {
          label: 'Desactivar pagos recurrentes',
          icon: 'pi pi-fw pi-calendar-minus',
          routerLink: ['/deactivate-recurrent-payments'],
        }
      ],
    };

    this.businessMgmtItems = {
      label: 'Gestión Empresarial',
      icon: 'pi pi-fw pi-arrow-up-right',
      items: [
        {
          label: 'Gestión de Usuarios',
          icon: 'pi pi-fw pi-user-edit',
          routerLink: ['/users'],
        },
        {
          label: 'Gestión de Cuentas',
          icon: 'pi pi-fw pi-sliders-v',
          routerLink: ['/accounts'],
        },
      ],
    };

    this.balanceItems = {
      label: 'Posición Consolidada',
      icon: 'pi pi-fw pi-wallet',
      routerLink: ['/consolidated'],
    };

    this.accountingItems = {
      label: 'Contabilidad',
      icon: 'pi pi-fw pi-chart-line',
      items: [
        {
          label: 'Configuración Libro Mayor',
          icon: 'pi pi-fw pi-cog',
          routerLink: ['/accounting/config'],
        },
        {
          label: 'Reportes Libro Mayor',
          icon: 'pi pi-fw pi-chart-bar',
          routerLink: ['/accounting/report'],
        },
      ],
    };

    this.collectionItems = {
      label: 'Cobros',
      icon: 'pi pi-fw pi-chart-pie',
      items: [
        {
          label: 'Configuración de cobros',
          icon: 'pi pi-fw pi-cog',
          routerLink: ['/collections/config'],
        },
        {
          label: 'Habilitacion de cobros',
          icon: 'pi pi-fw pi-check-square',
          routerLink: ['/collections/enable'],
        },
        {
          label: 'Lista de cobros',
          icon: 'pi pi-fw pi-list',
          routerLink: ['/collections/list'],
        },
        {
          label: 'Reporte de cobros',
          icon: 'pi pi-fw pi-cloud-download',
          routerLink: ['/collections/reports'],
        },
      ],
    };

    this.paymentsItems = {
      label: 'Pagos',
      icon: 'pi pi-fw pi-dollar',
      items: [
        {
          label: 'Carga de la contrapartida para pagos',
          icon: 'pi pi-fw pi-fast-forward',
          routerLink: ['/payments/loadorder'],
        },
        {
          label: 'Aprobación de pagos',
          icon: 'pi pi-fw pi-thumbs-up',
          routerLink: ['/payments/aprove'],
        },
        {
          label: 'Reporte de pagos',
          icon: 'pi pi-fw pi-ticket',
          routerLink: ['/payments/ordersreport'],
        },
        {
          label: 'Reporte de movimientos',
          icon: 'pi pi-fw pi-cloud-download',
          routerLink: ['/payments/transactionsreport'],
        },
      ],
    };

    this.rolePermissions = new Map<UserRoles, MenuItem[]>();

    this.rolePermissions.set(UserRoles.CUSTOMER, [
      this.customerServicesItems
    ]);

    this.rolePermissions.set(UserRoles.ADMINISTRADOR, [
      this.accountingItems,
      this.businessMgmtItems,
      this.balanceItems,
      this.collectionItems,
      this.paymentsItems,
    ]);

    // Todo menos Accounting
    this.rolePermissions.set(UserRoles.REPRESENTANTE_LEGAL, [
      this.businessMgmtItems,
      this.balanceItems,
      this.collectionItems,
      this.paymentsItems,
    ]);

    this.rolePermissions.set(UserRoles.INGRESADOR, [
      {
        ...this.collectionItems,
        items: this.buildIngresadorMenuFromItems(this.collectionItems.items),
      },
      {
        ...this.paymentsItems,
        items: this.buildIngresadorMenuFromItems(this.paymentsItems.items),
      },
    ]);

    this.rolePermissions.set(UserRoles.APROBADOR, [
      {
        ...this.collectionItems,
        items: this.buildApproverMenuFromItems(this.collectionItems.items),
      },
      {
        ...this.paymentsItems,
        items: this.buildApproverMenuFromItems(this.paymentsItems.items),
      },
    ]);
  }

  ngOnInit() {
    if (this.authService.authenticatedUser.role === UserRoles.CUSTOMER) {
      this.model = [
        {
          label: 'BANCA WEB',
          items: [
            ...this.rolePermissions.get(this.authService.authenticatedUser.role),
          ],
        },
      ];
    } else {
      this.model = [
        {
          label: 'CASH MANAGEMENT',
          items: [
            ...this.rolePermissions.get(this.authService.authenticatedUser.role),
          ],
        },
      ];
    }
  }

  private buildIngresadorMenuFromItems(items: MenuItem[]): MenuItem[] {
    return items.filter(({ routerLink }) => {
      const path: string = routerLink[0];
      return !matchesAproverPath(path) && !matchesReportPath(path);
    });
  }

  private buildApproverMenuFromItems(items: MenuItem[]): MenuItem[] {
    return items.filter(({ routerLink }) => {
      const path: string = routerLink[0];
      return (
        matchesAproverPath(path) ||
        matchesReportPath(path) ||
        !matchesConfigPath(path)
      );
    });
  }

  onKeydown(event: KeyboardEvent) {
    const nodeElement = <HTMLDivElement>event.target;
    if (event.code === 'Enter' || event.code === 'Space') {
      nodeElement.click();
      event.preventDefault();
    }
  }
}
