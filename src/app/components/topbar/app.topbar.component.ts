import { Component, OnInit, OnDestroy } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { MainLayoutComponent } from '../../layouts/app.main.component';

import { ConfigService } from 'src/app/services/utils/app.config.service';
import { AppConfig } from 'src/app/config/appconfig';
import { Subscription } from 'rxjs';
import { AuthenticationService } from 'src/app/services/utils/authentication.service';
import { User } from 'src/app/types/clients/User';
import { UserRoleNames } from 'src/app/types/user-roles'

@Component({
    selector: 'app-topbar',
    templateUrl: './app.topbar.component.html',
})
export class AppTopBarComponent implements OnInit, OnDestroy {
    itemsUser: MenuItem[] = [];

    config: AppConfig;

    subscription: Subscription;

    user: User;

    constructor(
        public appMain: MainLayoutComponent,
        public configService: ConfigService,
        private authService: AuthenticationService
    ) {}

    ngOnInit(): void {
        this.user = this.authService.authenticatedUser;

        this.config = this.configService.config;
        this.subscription = this.configService.configUpdate$.subscribe(
            (config) => {
                this.config = config;
            }
        );

        this.itemsUser = [
            { label: this.user.name, disabled:true },
            { label: UserRoleNames[this.user.role], disabled:true },
            { separator: true },
            {
                label: 'Cerrar Sesión',
                icon: 'pi pi-power-off',
                command: () => {
                    this.logout();
                },
                routerLink: ['/pages/login'],
            },
        ];
    }

    ngOnDestroy(): void {
        this.subscription.unsubscribe();
    }

    toggleDarkMode(): void {
        const themeElement = document.getElementById('theme-css');
        if (this.config.dark) {
            const theme: string = 'md-light-indigo';
            const dark: boolean = false;
            themeElement.setAttribute(
                'href',
                'assets/theme/md-light-indigo/theme.css'
            );
            localStorage.setItem(
                'config',
                JSON.stringify({ ...this.config, ...{ theme, dark } })
            );
            this.configService.updateConfig({
                ...this.config,
                ...{ theme, dark },
            });
        } else {
            const theme: string = 'md-dark-indigo';
            const dark: boolean = true;
            themeElement.setAttribute(
                'href',
                'assets/theme/md-dark-indigo/theme.css'
            );
            localStorage.setItem(
                'config',
                JSON.stringify({ ...this.config, ...{ theme, dark } })
            );
            this.configService.updateConfig({
                ...this.config,
                ...{ theme, dark },
            });
        }
    }

    logout() {
        this.authService.close();
    }
}
