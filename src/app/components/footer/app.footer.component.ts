import { Component } from '@angular/core';
import { MainLayoutComponent } from '../../layouts/app.main.component';

@Component({
    selector: 'app-footer',
    templateUrl: './app.footer.component.html'
})
export class AppFooterComponent{
    constructor(public appMain: MainLayoutComponent) {}
}
