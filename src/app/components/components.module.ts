import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { RadioButtonModule } from 'primeng/radiobutton';
import { InputSwitchModule } from 'primeng/inputswitch';

import { AppTopBarComponent } from './topbar/app.topbar.component';
import { AppMenuComponent } from './sidemenu/app.menu.component';
import { AppMenuitemComponent } from './sidemenu/app.menuitem.component';
import { AppFooterComponent } from './footer/app.footer.component';
import { ButtonModule } from 'primeng/button';
import { SplitButtonModule } from 'primeng/splitbutton';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    RadioButtonModule,
    InputSwitchModule,
    ButtonModule,
    SplitButtonModule
  ],
  declarations: [
    AppTopBarComponent,
    AppMenuComponent,
    AppMenuitemComponent,
    AppFooterComponent
  ],
  exports: [
    AppTopBarComponent,
    AppMenuComponent,
    AppMenuitemComponent,
    AppFooterComponent
  ]
})

export class ComponentsModule {}
