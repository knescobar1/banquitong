import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { MessageService } from 'primeng/api';
import { AccountService } from 'src/app/services/account/account.service';
import { CmAccountService } from 'src/app/services/clients/cm-account.service';
import { CollectionOrderService } from 'src/app/services/collections/collection-order.service';
import { CollectionService } from 'src/app/services/collections/collection.service';
import { AuthenticationService } from 'src/app/services/utils/authentication.service';
import { AccountCM } from 'src/app/types/clients/AccountCM';
import { CollectionOrder } from 'src/app/types/collections/CollectionOrder';

@Component({
  selector: 'app-deactivate-recurrent-payments',
  templateUrl: './deactivate-recurrent-payments.component.html',
  styleUrls: ['./deactivate-recurrent-payments.component.scss']
})
export class DeactivateRecurrentPaymentsComponent implements OnInit {

  collectionOrders: CollectionOrder[] = [];
  account: AccountCM;

  constructor(
    private messageService: MessageService,
    private collectionService: CollectionService,
    private collectionOrderService: CollectionOrderService,
    private authenticationService: AuthenticationService,
    private accountService: CmAccountService,
    private spinner: NgxSpinnerService,
    private cAccountService: AccountService
  ) { }

  ngOnInit(): void {
    this.spinner.show();
    this.getDataAccount();
  }

  getDataAccount() {
    this.cAccountService.obtainActivesByGroup(this.authenticationService.authenticatedUser.groupInternalId)
      .subscribe(
        res => {
          if(res.length > 0) {
            this.account = res[0];
            this.getOrders();
          }
        }
      );
  }

  getOrders() {
    this.collectionOrderService.obtainOrdersByDebtorAccountAndPayWay(this.account.accountNumber, 'REC').subscribe(
      res => {
        console.log(res);
        this.collectionOrders = res;
        this.spinner.hide();
      },
      err => {
        this.messageService.add({key: 'gl', severity:'error', summary:'Error', detail:'Error al obtener los pagos recurrentes'});
        this.spinner.hide();
      }
    ); 
  }

  deactivateRecurrentPayment(order: CollectionOrder) {
    this.collectionOrderService.changePaymentWay(order.internalId).subscribe(
      res => {
        this.getOrders();
        this.messageService.add({key: 'gl', severity:'info', summary:'Éxito', detail:'Pago programado desactivado'});
      },
      err => {
        this.messageService.add({key: 'gl', severity:'error', summary:'Error', detail:'Error al desactivar el pago programado'});
      }
    );
  }

}
