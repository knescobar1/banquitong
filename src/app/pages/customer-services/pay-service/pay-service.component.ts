import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ConfirmationService, MessageService } from 'primeng/api';
import { Observable } from 'rxjs';
import { AccountService } from 'src/app/services/account/account.service';
import { CmAccountService } from 'src/app/services/clients/cm-account.service';
import { CollectionOrderService } from 'src/app/services/collections/collection-order.service';
import { CollectionService } from 'src/app/services/collections/collection.service';
import { ServiceOfferedService } from 'src/app/services/payments/service-offered.service';
import { AuthenticationService } from 'src/app/services/utils/authentication.service';
import { AccountCM } from 'src/app/types/clients/AccountCM';
import { Collection } from 'src/app/types/collections/Collection';
import { CollectionOrder } from 'src/app/types/collections/CollectionOrder';
import { ServiceOffered } from 'src/app/types/payments/ServiceOffered';

@Component({
  selector: 'app-pay-service',
  templateUrl: './pay-service.component.html',
  styleUrls: ['./pay-service.component.scss'],
  providers: [ConfirmationService]
})
export class PayServiceComponent implements OnInit {

  accountsByGroupId$: Observable<AccountCM[]>;

  collectionsReq: Collection[];
  cmAccounts: AccountCM[];
  services: string[];
  selectedService: string;

  business: string;
  referenceid: string;
  amount: string;
  description: string;
  email: string;

  accounts: string[]=[''];
  selectedAccount: string;

  enterToggle: boolean = false;

  collectionOrders: CollectionOrder[] = [];
  account: AccountCM = {};

  constructor(
      private messageService: MessageService,
      private collectionService: CollectionService,
      private serviceOfferedService: ServiceOfferedService,
      private accountService: CmAccountService,
      private authenticationService: AuthenticationService,
      private router: Router,
      private confirmationService: ConfirmationService,
      private collectionOrderService: CollectionOrderService,
      private cAccountService: AccountService,
      private spinner: NgxSpinnerService,
  ) {
    this.accountsByGroupId$ = this.accountService.listAccountsByGroupInternalId(this.authenticationService.authenticatedUser.groupInternalId);
  }

  serviceChange(): void {
    this.business = this.collectionsReq.find(tempCol => tempCol.serviceOffered.name === this.selectedService).serviceOffered.description;
  }

  ngOnInit(): void {
    this.spinner.show();
    this.getDataAccount();
  }

  getDataAccount() {
    this.cAccountService.obtainActivesByGroup(this.authenticationService.authenticatedUser.groupInternalId)
      .subscribe(
        res => {
          if(res.length > 0) {
            this.account = res[0];
            this.getOrders();
          }
        }
      );
  }

  getOrders() {
    this.collectionOrderService.obtainOrdersByDebtorAccount(this.account.accountNumber).subscribe(
      res => {
        this.collectionOrders = res;
        this.spinner.hide();
      },
      err => {
        this.messageService.add({key: 'gl', severity:'error', summary:'Error', detail:'Error al obtener los pagos pendientes'});
        this.spinner.hide();
      }
    );
  }

  confirmPayment(order: CollectionOrder) {
    this.confirmationService.confirm({
      message: '¿Estás seguro de pagar este servicio?',
      header: 'Confirmación',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.collectionOrderService.payOrderCollection(order.internalId).subscribe(
          res => {
            this.messageService.add({key: 'gl', severity:'success', summary:'Correcto', detail:'Pago realizado con éxito'});
          },
          err => {
            console.log(err.message);
            this.messageService.add({key: 'gl', severity:'error', summary:'Error', detail:'Error al realizar el pago'});
          }
        );
        this.getOrders();
      },
      reject: () => {
        // this.messageService.add({key: 'gl', severity:'error', summary:'Error', detail:'Error al procesar el pago'});
      }
    });
  }

}
