import { Component, OnInit } from '@angular/core';
import { MessageService } from 'primeng/api';
import { PaymentOrderService } from 'src/app/services/payments/payment-order.service';
import { PaymentService } from 'src/app/services/payments/payment.service';
import { PaymentOrder } from '../../../../types/payments/PaymentOrder';
import { Payment } from '../../../../types/payments/Payment';
import { NgForm } from '@angular/forms';
import { AuthenticationService } from 'src/app/services/utils/authentication.service';
import { CmAccountService } from 'src/app/services/clients/cm-account.service';
import { AccountCM } from 'src/app/types/clients/AccountCM';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-paymentref-loadorder',
  templateUrl: './paymentref-loadorder.component.html',
  providers: [MessageService]
})
export class PaymentrefLoadorderComponent implements OnInit {

  accountsByGroupId$: Observable<AccountCM[]>;

  isLoading: boolean = false;
  paymentAddedOrder: PaymentOrder;
  cmAccounts: AccountCM[];

  paymentsOrder: PaymentOrder[] = [];
  payment: Payment;

  uploadedFiles: any[] = [];
  upload: File;

  isPaymentCreated: boolean = false;

  services: string[];
  selectedService: string = "";
  accounts: string[] = new Array(20);
  selectedAccount: string;
  displayLoad: boolean;
  reference: string = '';
  processDate: Date = new Date();
  keyReadyToggle = false;
  referencecont: string = "";
  accountForm: string = "";
  payValue: string = "";
  accountType: string[];
  selectedAccountType: string;
  identificationForm: string = "";
  idType: string[];
  selectedIdType: string;
  emailForm: string = "";
  benef: string = "";
  description: string = "";

  uploadToggle = false;

  constructor(
    private paymentOrderService: PaymentOrderService,
    private paymentservice: PaymentService,
    private messageService: MessageService,
    private accountService: CmAccountService,
    private authService: AuthenticationService) {
    this.accountsByGroupId$ = this.accountService.listAccountsByGroupInternalId(this.authService.authenticatedUser.groupInternalId);
    this.services = ['P-PAGO NOMINA', 'P-PAGO PROVEEDORES', 'P-PAGO A TERCEROS'];
    this.selectedService = this.services[0];
    this.accountType = ['Cuenta de ahorro', 'Cuenta corriente'];
    this.selectedAccountType = this.accountType[0];
    this.idType = ['Cédula', 'Pasaporte', 'Otro'];
    this.selectedIdType = this.idType[0];
  }

  ngOnInit(): void {
    this.accountsByGroupId$.subscribe(
      accounts_cm => {
        this.cmAccounts = accounts_cm;
        this.accounts = this.cmAccounts.map(account => (account.serviceType === 'PAY' || account.serviceType === 'PNC') ? account.accountNumber.toString() : '');
        this.accounts = this.accounts.filter(account => account !== "");
        this.selectedAccount = this.accounts[0];
        this.isLoading = false;
      },
      err => {
        console.error(err);
        this.showFetchError();
      }
    );
  }

  showLoadDialog(): void {
    this.displayLoad = true;
  }

  uploadFile(event: any, form: NgForm): void {
    this.upload = event.files[0];
    this.paymentOrderService.validateCSVFile(event.files[0]).subscribe(
      res => {
        this.messageService.add({ key: 'toast-payments', severity: 'info', summary: 'Éxito', detail: 'Su archivo es valido' });
        this.uploadToggle = true;
        form.reset();
      }, ({ error }) => {
        this.upload = undefined;
        this.messageService.add({ key: 'toast-payments', severity: 'error', summary: 'Error', detail: error?.message ?? 'Su archivo no es válido.' });
      });
  }

  generatePay(): void {
    this.payment = {
      internalId: '',
      groupInternalId: this.authService.authenticatedUser.groupInternalId,
      serviceName: this.selectedService,
      serviceDescription: this.reference,
      state: 'PEN',
      debtorAccount: parseInt(this.selectedAccount),
      processDate: this.processDate.toISOString(),
      records: parseInt("0"),
      paidRecords: parseInt("0"),
      failedRecords: parseInt("0"),
      totalPaymentValue: parseFloat("0.00"),
      paidValue: parseFloat("0.00"),
      failedValue: parseFloat("0.00"),
      journalId: ''
    };

    this.paymentservice.create(this.payment).subscribe({
      next: (payment) => {
        this.payment = payment;
        this.isPaymentCreated = true;
      },
      error: () => {
        this.messageService.add({ key: 'toast-payments', severity: 'error', summary: 'Error', detail: 'No se ha podido crear su pago' });
        this.showFetchError();
      },
      complete: () => {
        this.isLoading = false;
      },
    });
  }

  keyboardSubmit(): void {
    const reqPaymentOrder: PaymentOrder = {
      internalId: '',
      referenceId: this.referencecont,
      paymentId: this.payment.internalId,
      creditorIdentification: this.identificationForm,
      creditorIdentificationType: this.selectedIdType === 'Cédula' ? 'CED' : (this.selectedIdType === 'Pasaporte' ? 'PAS' : 'OTR'),
      creditorName: this.benef,
      creditorAccount: parseInt(this.accountForm),
      creditorAccountType: this.selectedAccountType === 'Cuenta de ahorro' ? 'AHO' : 'COR',
      creditorEmail: this.emailForm,
      amount: parseFloat(this.payValue),
      state: 'PEN',
      description: this.description,
      paymentDate: ''
    };

    this.keyReadyToggle = true;
    this.isLoading = true;
    this.paymentOrderService.create(reqPaymentOrder, this.payment.internalId).subscribe(() => {
      this.messageService.add({ key: 'toast-payments', severity: 'info', summary: 'Éxito', detail: 'Su órden de pago es procesado exitosamente' });
      this.paymentsOrder.push(reqPaymentOrder);
      this.keyReadyToggle = false;
      this.isLoading = false;
    });
  }

  processFile(): void {
    this.isLoading = true;
    this.paymentOrderService.createOrdersByCsv(this.upload, this.payment.internalId).subscribe(payments => {
      this.messageService.add({ key: 'toast-payments', severity: 'info', summary: 'Éxito', detail: 'Su archivo es procesado exitosamente' });
      this.paymentsOrder = [...this.paymentsOrder, ...payments];
      //this.paymentOrderService.obtainByPaymentId(this.payment.internalId).subscribe(payFile => this.paymentsOrder.push(payFile));
      this.uploadToggle = false;
    });
  }



  cancelKeyboardSubmited(): void {
    this.paymentsOrder = [];
    this.keyReadyToggle = false;
  }

  private showFetchError(): void {
    this.messageService.add({
      severity: 'error',
      summary: 'Error',
      detail: 'Error al crear el pago.',
    });
  }
}