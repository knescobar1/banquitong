import { Component, OnInit } from '@angular/core';
import { MessageService } from 'primeng/api';
import { PaymentService } from 'src/app/services/payments/payment.service';
import { AuthenticationService } from 'src/app/services/utils/authentication.service';
import { Payment } from 'src/app/types/payments/Payment';

@Component({
  selector: 'app-payments-orders-report',
  templateUrl: './payments-orders-report.component.html'
})
export class PaymentsOrdersReportComponent implements OnInit {

  paymentsOrders: Payment[];
  toggleReport: boolean = false;
  services: string[];
  selectedService: string = "";
  orderStates: string[];
  orderState: string = "";
  initDate: Date;
  finishDate: Date;
  idOrder: string = "";
  reference: string = "";

  orderStateQueried: string = "";

  constructor(private messageService: MessageService,
    private paymentService: PaymentService,
    private authService: AuthenticationService) {
    this.services = ['P-PAGO NOMINA', 'P-PAGO PROVEEDORES', 'P-PAGO A TERCEROS'];
    this.orderStates = ["APROBADO", "PENDIENTE"];
  }

  ngOnInit(): void {
    this.selectedService = this.services[0];
    this.orderState = this.orderStates[0];
  }

  query(): void {
    this.paymentService.obtainPaymentsByProperties(
      this.authService.authenticatedUser.groupInternalId,
      this.idOrder,
      this.selectedService,
      this.orderState,
      this.initDate,
      this.finishDate,
      this.reference,
    ).subscribe({
      next: (payments) => {
        this.toggleReport = true;
        this.paymentsOrders = payments;
        this.orderStateQueried = this.orderState;
      },
      error: (err) => {
        this.messageService.add({
          severity: 'error',
          summary: 'Error',
          detail: err?.message ?? 'Algo ha salido mal'
        });
      },
      complete: () => {
        // this.isLoading = false;
      },
    });
  }

}
