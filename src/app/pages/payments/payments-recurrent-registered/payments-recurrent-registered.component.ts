import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-payments-recurrent-registered',
  templateUrl: './payments-recurrent-registered.component.html',
  styles: [
  ]
})
export class PaymentsRecurrentRegisteredComponent implements OnInit {

  constructor() { }

  data: any[] = [
    {
      reference: "1",
      service: "2",
      channel: "Activo",
      periodic: "04/04/2020",
      account: "05/04/2020",
    }
  ];

  ngOnInit(): void {
  }

}
