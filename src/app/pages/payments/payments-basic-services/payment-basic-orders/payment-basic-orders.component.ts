import { Component, OnInit } from '@angular/core';
import { ServiceOfferedService } from 'src/app/services/payments/service-offered.service';
import { ServiceOffered } from '../../../../types/payments/ServiceOffered';
import { Observable } from 'rxjs';
import { NgxSpinnerService } from 'ngx-spinner';
import { MessageService } from 'primeng/api';
import { AuthenticationService } from 'src/app/services/utils/authentication.service';
import { CollectionOrderService } from 'src/app/services/collections/collection-order.service';
import { CollectionOrder } from 'src/app/types/collections/CollectionOrder';
import { UserRoles } from 'src/app/types/user-roles';

@Component({
  selector: 'app-payment-basic-orders',
  templateUrl: './payment-basic-orders.component.html'
})
export class PaymentBasicOrdersComponent implements OnInit {

  ª

  pendingServices$: Observable<ServiceOffered[]>
  displayLoad: boolean = false;
  servicesOff: ServiceOffered[];
  collectionSum: CollectionOrder[]=[];

  serviceOfferToPay: ServiceOffered={
    internalId: '',
    groupInternalId: '',
    referenceId: '',
    creditorAccount: 0,
    creditorIdentification: '',
    creditorEmail: '',
    debtorAccount: 0,
    serviceName: '',
    description: '',
    maxAmount: 0,
    aprobationState: '',
    recurrentState: '', 
    frecuency: '', 
    firstScheduleDate: '',
    finalScheduleDate: '',
    lastPaymentDate: '',
  };

  whoAmI: UserRoles;

  bussinessToPay: string;
  serviceToPay: string;
  referenceIdToPay: string;
  accountDebToPay: string;
  descriptionToPay: string;
  emailToPay: string;
  generalAmount: number;

  constructor(
    private messageService: MessageService,
    private serviceOfferedService: ServiceOfferedService,
    private authService: AuthenticationService,
    private collectionOrderService: CollectionOrderService,
    private spinner: NgxSpinnerService) {
    this.pendingServices$ = this.serviceOfferedService.obtainPendingServicesOffered(this.authService.authenticatedUser.groupInternalId);
  }

  ngOnInit(): void {
     this.spinner.show();
     this.whoAmI = this.authService.authenticatedUser.role;
     console.log(this.whoAmI);
     this.pendingServices$.subscribe(serv => {
        this.servicesOff = serv;
        this.spinner.hide();
    });
  }

  approve(serv: ServiceOffered): void {
    this.serviceOfferedService.approve(serv.internalId).subscribe(
      res => {
        this.messageService.add({ key: 'toast-payments', severity: 'info', summary: 'Éxito', detail: 'Su pago ha sido aprobado con éxito' });
        this.servicesOff = this.servicesOff.filter(p => p.internalId !== serv.internalId);
      },
      err => {
        this.messageService.add({ key: 'toast-payments', severity: 'error', summary: 'Error', detail: 'No se ha podido aprobar su pago' });
      }
    );
  }

  payDialog(srv: ServiceOffered): void {
    this.collectionSum = [];
    let collectionsOrders: CollectionOrder[]=[];
    this.displayLoad = true;
    this.serviceOfferToPay = srv;
    console.log(srv.serviceName);
    console.log(srv.referenceId);
    this.collectionOrderService.obtainByCollectionServiceName(srv.serviceName, srv.referenceId).subscribe(cols => {
      this.collectionSum.push(cols[cols.length -1]);
    });
  }

  generalAssign(am: number): void {
    this.generalAmount = am;
  }

  pay(): void {
    const totalPay = this.collectionSum[0].amount + 0.4;
    this.serviceOfferedService.payServiceOffered(this.serviceOfferToPay.internalId, totalPay).subscribe(
      res => {
        this.messageService.add({ key: 'toast-payments', severity: 'info', summary: 'Éxito', detail: 'Se ha pagado con éxito' });
        this.displayLoad = false;
      },
      err => {
        console.log(err);
        this.messageService.add({ key: 'toast-payments', severity: 'error', summary: 'Error', detail: 'No se ha podido compleatar su pago' });
        this.displayLoad = false;
      }
    );
  }

}