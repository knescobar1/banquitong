import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { CollectionService } from 'src/app/services/collections/collection.service';
import { ServiceOfferedService } from 'src/app/services/payments/service-offered.service';
import { Collection } from 'src/app/types/collections/Collection';
import { ServiceOffered } from '../../../../types/payments/ServiceOffered';
import { AuthenticationService } from '../../../../services/utils/authentication.service';
import { AccountCM } from 'src/app/types/clients/AccountCM';
import { Observable } from 'rxjs';
import { CmAccountService } from 'src/app/services/clients/cm-account.service';

@Component({
  selector: 'app-payment-basic-add',
  templateUrl: './payment-basic-add.component.html',
})
export class PaymentBasicAddComponent implements OnInit {

  accountsByGroupId$: Observable<AccountCM[]>;

  collectionsReq: Collection[];
  cmAccounts: AccountCM[];
  services: string[];
  selectedService: string;

  business: string;
  referenceid: string;
  amount: string;
  description: string;
  email: string;

  accounts: string[]=[''];
  selectedAccount: string;

  enterToggle: boolean = false;

  constructor(
      private messageService: MessageService,
      private collectionService: CollectionService,
      private serviceOfferedService: ServiceOfferedService,
      private accountService: CmAccountService,
      private authenticationService: AuthenticationService,
      private router: Router
  ) {
    this.accountsByGroupId$ = this.accountService.listAccountsByGroupInternalId(this.authenticationService.authenticatedUser.groupInternalId);
  }

  serviceChange(): void {
    this.business = this.collectionsReq.find(tempCol => tempCol.serviceOffered.name === this.selectedService).serviceOffered.description;
  }

  ngOnInit(): void {
    this.accountsByGroupId$.subscribe(
      accounts_cm => {
        this.cmAccounts = accounts_cm;
        this.accounts = this.cmAccounts.map(account => (account.serviceType === 'COL') ? account.accountNumber.toString() : '');
        this.accounts = this.accounts.filter(account => account !== "");
        this.selectedAccount = this.accounts[0];
      }
    );

    this.collectionService.obtainServices().subscribe(
      cols => {
        this.collectionsReq = cols;
        this.services = cols.map(tempCol => tempCol.serviceOffered.name);
      }, err => {
				this.messageService.add({
					severity: 'error',
					summary: 'Error',
					detail: err?.message ?? 'Algo ha malido sal'
        })
      });
  }

  enterButton(): void {
    this.enterToggle = !this.enterToggle;
  }

  saveButton(): void {
    const serviceOffer: ServiceOffered = {
      internalId: '',
      groupInternalId: this.authenticationService.authenticatedUser.groupInternalId,
      referenceId: this.referenceid,
      creditorAccount: 0,
      creditorIdentification: 'CED',
      creditorEmail: this.email,
      debtorAccount: 122545345,
      serviceName: this.selectedService,
      description: this.description,
      maxAmount: parseFloat(this.amount),
      aprobationState: 'PEN',
      lastPaymentDate: new Date().toISOString(),
      recurrentState: '',
      frecuency: '',
      firstScheduleDate: '',
      finalScheduleDate: '',
    };
    this.serviceOfferedService.create(serviceOffer);
    this.router.navigateByUrl('/payments/basicorders');
  }

}
