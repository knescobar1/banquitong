import { Component } from '@angular/core';
import { MessageService } from 'primeng/api';
import { PaymentOrderService } from 'src/app/services/payments/payment-order.service';
import { AuthenticationService } from 'src/app/services/utils/authentication.service';
import { PaymentOrder } from 'src/app/types/payments/PaymentOrder';

@Component({
  selector: 'app-payments-transactions-report',
  templateUrl: './payments-transactions-report.component.html'
})
export class PaymentsTransactionsReportComponent {

  paymentsOrders: PaymentOrder[];
  toggleReport: boolean = false;

  constructor(private messageService: MessageService,
    private paymentOrderService: PaymentOrderService,
    private authService: AuthenticationService) {
    this.orderState = ["REALIZADO", "PENDIENTE", "FALLIDO"];
    this.selectedOrderState = this.orderState[0];
  }

  orderState: string[];
  selectedOrderState: string = "";
  initDate: Date = new Date();
  finishDate: Date = new Date();
  idOrder: string = "";
  orderStateQueried: string = "";

  dateToggle: boolean = true;

  isPen(): void {
    if (this.selectedOrderState === "PENDIENTE" || this.selectedOrderState === "FALLIDO") {
      this.dateToggle = false;
      this.initDate = undefined;
      this.finishDate = undefined;
    } else {
      this.dateToggle = true;
      this.initDate = new Date();
      this.finishDate = new Date();
    }
  }

  query(): void {
    let initDateCtrl;
    let finDateCtrl;
    if (this.initDate === undefined) initDateCtrl = null;
    else initDateCtrl = this.initDate;
    if (this.finishDate === undefined) finDateCtrl = null;
    else finDateCtrl = this.finishDate;
    const movement: string = this.selectedOrderState === "REALIZADO" ? "DON" : (this.selectedOrderState === "PENDIENTE" ? "PEN" : "FAI");
    console.log(movement);
    this.toggleReport = true;
    this.paymentOrderService.obtainOrdersByProperties(this.authService.authenticatedUser.groupInternalId,
      movement, this.idOrder, initDateCtrl, finDateCtrl)
      .subscribe(rep => {
        console.log(rep);
        this.toggleReport = true;
        this.paymentsOrders = rep;
        this.orderStateQueried = this.selectedOrderState;
      }, err => {
        this.messageService.add({
          severity: 'error',
          summary: 'Error',
          detail: err?.message ?? 'Algo ha salido mal'
        });
      }
      );
  }
}
