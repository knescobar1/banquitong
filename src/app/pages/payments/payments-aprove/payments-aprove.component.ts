import { Component } from '@angular/core';
import { Observable } from 'rxjs';

import { Payment } from '../../../types/payments/Payment';
import { PaymentService } from '../../../services/payments/payment.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { MessageService } from 'primeng/api';
import { AuthenticationService } from 'src/app/services/utils/authentication.service';

@Component({
  selector: 'app-payments-aprove',
  templateUrl: './payments-aprove.component.html'
})
export class PaymentsAproveComponent {

  getPayments$: Observable<Payment[]>;

  constructor(
    private paymentService: PaymentService,
    private messageService: MessageService,
    private spinner: NgxSpinnerService,
    private authService: AuthenticationService) {
    this.service = ['P-PAGO NOMINA', 'P-PAGO PROVEEDORES', 'P-PAGO A TERCEROS'];
    this.selectedService = this.service[0];
  }

  queryState: boolean = false;

  payments: Payment[];

  service: string[];
  selectedService: string = "";

  initDate: Date = new Date();
  finishDate: Date = new Date();

  query(): void {
    this.spinner.show();
    this.getPayments$ = this.paymentService.obtainPaymentsByState('PEN', this.authService.authenticatedUser.groupInternalId, this.selectedService, this.initDate, this.finishDate);
    this.getPayments$.subscribe(col => {
      this.payments = col;
      this.queryState = true;
      this.spinner.hide();
    });
  }

  approve(pay: Payment): void {
    this.paymentService.approve(pay.internalId, this.authService.authenticatedUser.mail).subscribe(
      res => {
        this.messageService.add({ key: 'toast-payments', severity: 'info', summary: 'Éxito', detail: 'Su pago ha sido aprobado con éxito' });
        this.payments = this.payments.filter(p => p.internalId !== pay.internalId);
      },
      err => {
        this.messageService.add({ key: 'toast-payments', severity: 'error', summary: 'Error', detail: 'No se ha podido aprobar su pago' });
      }
    );
  }
}