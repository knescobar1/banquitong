import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-payment-program',
  templateUrl: './payment-program.component.html',
  styles: [
  ]
})
export class PaymentProgramComponent implements OnInit {

  constructor() { }

  data: any[] = [
    {
      reference: "1",
      service: "2",
      channel: "Activo",
      periodic: "04/04/2020",
      account: "05/04/2020",
    }
  ];


  ngOnInit(): void {
  }

}
