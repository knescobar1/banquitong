import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-payment-ready',
  templateUrl: './payment-ready.component.html',
  styles: [
  ]
})
export class PaymentReadyComponent implements OnInit {

  constructor() { }

  data: any[] = [
    {
      reference: "1",
      service: "2",
      channel: "Activo",
      periodic: "04/04/2020",
      account: "05/04/2020",
    }
  ];

  ngOnInit(): void {
  }

}
