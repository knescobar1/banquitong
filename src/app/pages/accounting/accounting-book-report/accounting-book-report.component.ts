import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { MessageService } from 'primeng/api';
import { JournalEntryService } from 'src/app/services/accounting/journal-entry.service';
import { GeneralLedgerReport } from 'src/app/types/accounting/GeneralLedgerReport';

@Component({
  selector: 'app-accounting-book-report',
  templateUrl: './accounting-book-report.component.html',
  styles: [
  ]
})
export class AccountingBookReportComponent implements OnInit {
  
  reportGLA: GeneralLedgerReport[] = [];
  from: Date;
  to: Date;
  cols: any;
  nameDocument: string;
  debitTotal: number;
  creditTotal: number;
  lastBalance: number;
  balanceTotal: number;
  validDates: boolean = false;

  constructor(
    private journalService: JournalEntryService,
    private messageService: MessageService,
    private spinner: NgxSpinnerService
    ) {
    this.cols = [
      {field: "accountCode", header: "Codigo"},
      {field: "accountingDate", header: "Fecha"},
      {field: "memo", header: "Concepto"},
      {field: "debit", header: "Debe"},
      {field: "credit", header: "Haber"},
      {field: "accountBalance", header: "Saldo Cuenta"}
    ]
  }

  ngOnInit(): void {

  }

  search() {
    if(this.to > this.from) {
      this.spinner.show();
      this.journalService.getReportByPeriod(this.from, this.to).subscribe({
          next: (res) => {
            console.log(res);
            
            if(res.length > 0) {
              this.reportGLA = res;
              this.nameDocument = `Reporte_Diario_Libro_Mayor_${this.from.toISOString().split('T')[0]}-${this.to.toISOString().split('T')[0]}`;
              this.calculateDebitTotal();
              this.calculateCreditTotal();
              this.calculateBalanceTotal();
              this.obtainLastBalance();
              this.validDates = true;
              this.spinner.hide();
            } else {
              this.spinner.hide();
              this.validDates = false;
              this.messageService.add({key: 'gl', severity:'info', summary:'Completado', detail:'No existen registros en el rango de fechas consultado'});
            }
          },
          error: (err) => {
            this.spinner.hide();
            this.validDates = false;
            this.messageService.add({key: 'gl', severity:'error', summary:'Error', detail:'Error al obtener el reporte: ' + err});
          }
        }
      );
    } else {
      this.spinner.hide();
      this.validDates = false;
      this.messageService.add({key: 'gl', severity:'warn', summary:'Error', detail:'El rango de fechas es incorrecto.'});
    }
  }

  calculateDebitTotal() {
    let total = 0;
    this.reportGLA.forEach(rep => {
      total += rep.debit;
    });

    this.debitTotal = total;
  }

  calculateCreditTotal() {
    let total = 0;
    this.reportGLA.forEach(rep => {
      total += rep.credit;
    });

    this.creditTotal = total;
  }

  calculateBalanceTotal() {
    let total = 0;
    this.reportGLA.forEach(rep => {
      total += rep.accountBalance;
    });

    this.balanceTotal = total;
  }

  obtainLastBalance() {
    this.lastBalance = this.reportGLA[this.reportGLA.length - 1].accountBalance;
  }

}
