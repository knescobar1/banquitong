import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Message, MessageService } from 'primeng/api';
import { GeneralLedgerAccountService } from 'src/app/services/accounting/general-ledger-account.service';
import { GeneralLedgerAccount } from 'src/app/types/accounting/GeneralLedgerAccount';

const typesAccount: any[] = [
  {
    "id": "ASSETS",
    "name": "Activos"
  },
  {
    "id": "EXPENSES",
    "name": "Gastos"
  },
  {
    "id": "INCOME",
    "name": "Ingresos"
  }
]

@Component({
  selector: 'app-accounting-book-config',
  templateUrl: './accounting-book-config.component.html',
  styles: [
  ]
})

export class AccountingBookConfigComponent implements OnInit {

  types: any[];
  generalLedger: GeneralLedgerAccount = {};
  validTypes: any[] = [];
  res: any[];
  existTypes: boolean = true;
  notification: Message[];
  accounts: GeneralLedgerAccount[] = [];

  constructor(
    private generalLedgerAccountService: GeneralLedgerAccountService,
    private messageService: MessageService,
    private router: Router
    ) {
    this.types = typesAccount;
  }

  ngOnInit(): void {
    this.getValidTypes();
    
    this.existTypes = (this.getValidTypes.length > 0) ? true : false;
    if(!this.existTypes) {
      this.notification = [
        {severity:'warn', summary:'Advertencia', detail:'Ya se han configurado las cuentas de todos los tipos posibles', closable:false}
      ];
    }
  }

  save() {
    this.generalLedgerAccountService.create(this.generalLedger).subscribe(
      () => {
        this.messageService.add({severity: 'success', summary: 'Éxito', detail: 'Configuración guardada correctamente'});
      },
      () => {
        setTimeout(() => {
          this.messageService.add({severity: 'error', summary: 'Error', detail: 'Error al guardar la Configuración del Libro Mayor'});
        }, 3000);
      },
      () => {
        this.router.navigate(['/']);
      }
    );
  }

  async getValidTypes() {
    this.types.forEach(type => {
      this.generalLedgerAccountService.getLedgerAccountByType(type.id).subscribe(
        (res) => {
          if(!res) {
            this.validTypes.push(type);
          } else {
            this.accounts.push(res);
          }
        }
      )
    });
  }

}
