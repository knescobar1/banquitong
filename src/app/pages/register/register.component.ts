import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { Subscription } from 'rxjs';

import { Group } from 'src/app/types/clients/Group';
import { CompanyService } from 'src/app/services/clients/company.service';
import { GroupService } from 'src/app/services/clients/group.service';
import { ConfigService } from 'src/app/services/utils/app.config.service';
import { RegisterCompanyValidator } from 'src/app/validators/register-company.validator';
import { AppConfig } from 'template/api/appconfig';
import { CreateCompany } from 'src/app/types/clients/CreateCompany';
import { AccountTypesEnum } from 'src/app/models/account-types.enum';
import { AccountService } from 'src/app/services/account/account.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
  styles: [
    `
      :host ::ng-deep .pi-eye {
        transform: scale(1.6);
        color: var(--primary-color) !important;
      }

      :host ::ng-deep .pi-eye-slash {
        transform: scale(1.6);
        color: var(--primary-color) !important;
      }
    `,
  ],
})
export class RegisterComponent implements OnInit {
  private numberRegex: RegExp = /^\d+$/;

  private config: AppConfig;
  private subscription: Subscription;
  private rucFieldSubscription: Subscription;
  private groupChangesSubscription: Subscription;

  public isRegisteringCompany: boolean = false;
  public companyAlreadyInCmClients: boolean = false;
  public groupIsNotPresentInCoreCustomers: boolean = false;

  public registerCompanyForm = this.formBuilder.group({
    ruc: [
      '',
      [Validators.required, Validators.minLength(13), Validators.maxLength(13)],
      [
        RegisterCompanyValidator.alreadyInCmClientsValidator(
          this.companyService
        ),
        RegisterCompanyValidator.groupInCoreCustomersValidator(
          this.groupService
        ),
      ],
    ],
    accountType: [
      { value: AccountTypesEnum.AHORROS, disabled: true },
      [Validators.required],
    ],
    accountNumber: [
      { value: '', disabled: true },
      [Validators.required, Validators.minLength(15), Validators.maxLength(15)],
    ],
  });

  public accountTypes: { name: string; value: AccountTypesEnum }[];

  public get group(): Group | null {
    return this.groupService.group;
  }

  public get isLoading(): boolean {
    return this.groupService.isLoading || this.companyService.isLoading;
  }

  constructor(
    // Third-party services
    public router: Router,
    private formBuilder: FormBuilder,
    private messageService: MessageService,
    // Services
    private accountService: AccountService,
    public configService: ConfigService,
    private companyService: CompanyService,
    private groupService: GroupService
  ) {
    this.accountTypes = [
      {
        name: 'Ahorros',
        value: AccountTypesEnum.AHORROS,
      },
      {
        name: 'Corriente',
        value: AccountTypesEnum.CORRIENTE,
      },
    ];
  }

  public ngOnInit(): void {
    this.config = this.configService.config;

    this.subscription = this.configService.configUpdate$.subscribe((config) => {
      this.config = config;
    });

    this.rucFieldSubscription =
      this.registerCompanyForm.controls.ruc.valueChanges.subscribe((_) => {
        if (this.registerCompanyForm.controls.ruc.value.length !== 13)
          this.groupService.group = null;
      });

    this.groupChangesSubscription = this.groupService.group$.subscribe(
      (group) => {
        if (group === null) {
          this.registerCompanyForm.controls.accountType.disable();
          this.registerCompanyForm.controls.accountNumber.disable();
          return;
        }

        this.registerCompanyForm.controls.accountType.enable();
        this.registerCompanyForm.controls.accountNumber.enable();
      }
    );
  }

  public ngOnDestroy(): void {
    this.subscription.unsubscribe();
    this.rucFieldSubscription.unsubscribe();
    this.groupChangesSubscription.unsubscribe();
  }

  public register() {
    this.registerCompanyForm.markAllAsTouched();

    const isInvalidForm: boolean = this.registerCompanyForm.invalid;
    if (isInvalidForm || this.groupService.group === null) {
      return;
    }

    const { ruc, accountType, accountNumber } = this.registerCompanyForm.value;
    const { internalId } = this.groupService.group;

    this.isRegisteringCompany = true;

    this.accountService
      .fetchGroupAccount({ groupId: internalId, accountType, accountNumber })
      .subscribe({
        next: (account) => {
          console.log(account);
          this.createCompany({
            companyRuc: ruc,
            groupInternalId: internalId,
          });
        },
        error: ({ error }) => {
          console.error(error);
          this.triggerErrorMessage(error?.message);
          this.isRegisteringCompany = false;
        },
      });
  }

  private createCompany(company: CreateCompany) {
    this.companyService.createCompany(company).subscribe({
      next: (res) => {
        console.log(res);
        this.isRegisteringCompany = false;

        this.triggerSucessMessage();
        this.groupService.group = null;
        this.registerCompanyForm.reset();
      },
      error: ({ error }) => {
        this.triggerErrorMessage(error?.message);

        this.isRegisteringCompany = false;
      },
    });
  }

  private triggerSucessMessage(): void {
    this.messageService.add({
      key: 'register-toast',
      severity: 'success',
      summary: 'Cuenta creada',
      detail: 'En breve recibirá un correo con sus credenciales.',
    });
  }

  private triggerErrorMessage(message: string | undefined): void {
    this.messageService.add({
      key: 'register-toast',
      severity: 'error',
      summary: 'Error',
      detail: message ?? 'Algo ha salido mal',
    });
  }

  public isInvalidField(fieldName: string): boolean {
    const field = this.registerCompanyForm.controls[fieldName];
    return field.touched && field.invalid;
  }

  public getFieldErrors(fieldName: string): string | null {
    const field = this.registerCompanyForm.controls[fieldName];

    if (field.hasError('companyAlreadyInCmClients'))
      return 'La empresa ya está registrada.';

    if (field.hasError('groupIsNotPresentInCoreCustomers'))
      return 'No existe un grupo registrado con ese RUC.';

    if (field.hasError('minlength')) {
      const minLength: number = field?.errors?.minlength.requiredLength;

      return `Este campo debe tener ${minLength} digitos.`;
    }

    if (field.hasError('maxlength')) {
      const maxLength: number = field?.errors?.maxlength.requiredLength;

      return `Este campo debe tener ${maxLength} digitos.`;
    }

    if (field.hasError('required')) return 'Este campo es necesario.';

    return null;
  }

  public acceptOnlyNumericValues(event: KeyboardEvent): boolean {
    const isNotNumericChar = !this.numberRegex.test(event.key);
    if (isNotNumericChar) {
      event.preventDefault();
      return false;
    }

    return true;
  }

  public isInvalidForm(): boolean {
    return this.registerCompanyForm.invalid;
  }
}
