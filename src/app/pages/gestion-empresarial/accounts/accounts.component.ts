import { Component, OnInit } from '@angular/core';
import { MessageService } from 'primeng/api';
import { AccountService } from 'src/app/services/account/account.service';
import { AuthenticationService } from 'src/app/services/utils/authentication.service';
import { Account } from '../../../types/accounts/Account';
import { CmAccountService } from '../../../services/clients/cm-account.service';
import { AccountCM } from '../../../types/clients/AccountCM';
import { ServiceTypes } from 'src/app/types/service-types';

@Component({
    selector: 'app-accounts',
    templateUrl: './accounts.component.html',
})
export class AccountsComponent implements OnInit {

    public accounts: Account[] = null;
    public accountsCM: AccountCM[] = null;
    public isLoading: boolean = false;
    
    selectedUse: any = null;

    city: string;

    uses: any[] = [
        { name: 'Pagos', key: 'P' },
        { name: 'Cobros', key: 'C' },
        { name: 'Pagos y Cobros', key: 'PC' }
    ];

    valRadio: string;

    account: Account;

    selectedAccount: Account;

    constructor(
      private messageService: MessageService,
      private authService: AuthenticationService,
      private coreAccountService: AccountService,
      private cmAccountService: CmAccountService,
    ) {}

    ngOnInit(): void {
      this.isLoading = true;

      this.listAccountsCM();

      this.coreAccountService.getAccountsByGroup(this.getGrroupInternalId()).subscribe({
            next: (accountList) => {
                this.accounts = accountList;
            },
            error: (err) => {
                console.error(err);
                this.showErrorMessage('No se pueden recuperar las cuentas');
            },
            complete: () => {
                this.isLoading = false;
            },
        });
    }

    private getGrroupInternalId(): string{
      const { groupInternalId } = this.authService.authenticatedUser;
      return groupInternalId;
    }
    
    private listAccountsCM(){
      this.cmAccountService.listAccountsByGroupInternalId(this.getGrroupInternalId()).subscribe({
        next: (accountCMList) => {
          accountCMList.forEach(account =>
            account.serviceType = ServiceTypes[account.serviceType]
          );
          this.accountsCM = accountCMList;
        },
        error: (err) => {
          console.error(err);
          this.showErrorMessage('No se pueden recuperar las cuentas');
        },
        complete: () => {
          this.isLoading = false;
        },
      });
    }

    private showErrorMessage(detail: string) {
      this.messageService.add({
        key: 'accounts-toast',
        severity: 'error',
        summary: 'Error',
        detail: detail,
      });
    }

    private showCreateMessage(detail: string) {
      this.messageService.add({
        key: 'accounts-toast',
        severity:'success',
        summary: 'Listo',
        detail: detail,
      });
    }

    public registerAccount() {
      const { groupInternalId } = this.authService.authenticatedUser;
      
      if(this.valRadio==null||this.selectedAccount==null){
        this.showErrorMessage('No se han seleccionado todos los campos necesarios');
      }else{
        let registerAccount: AccountCM = {
          accountNumber: this.selectedAccount.accountNumber,
          accountType : this.selectedAccount.accountType,
          serviceType : this.valRadio,
          accountingBalance: this.selectedAccount.balance,
          availableBalance: this.selectedAccount.balance,
          accountState: "ACTIVE",
          groupInternalId: groupInternalId
        };
        this.cmAccountService.findAccountByNumber(registerAccount).subscribe({
          next: (accountCM) => {
            this.modify(registerAccount);
          },
          error: (err) => {
            this.register(registerAccount);
          },
          complete: () => {
          },
        });
      }
    }

    public remove(accountCM: AccountCM){
      accountCM.serviceType = "NAS";
      this.modifyRemove(accountCM);
    }

    private register(newAccount: AccountCM){
      this.cmAccountService.registerAccountOnCm(newAccount).subscribe({
        next: (accountCM) => {
          //this.displayLoad = false;
          this.showCreateMessage('Cuenta asignada correctamente');
          this.listAccountsCM();
        },
        error: (err) => {
          console.error(err);
          this.showErrorMessage('El uso seleccionado ya ha sido asignado a otra cuenta');
        },
        complete: () => {
        },
      });
    }

    private modify(newAccount: AccountCM){
      this.cmAccountService.modifyAccount(newAccount).subscribe({
        next: (accountCM) => {
          //this.displayLoad = false;
          this.showCreateMessage('Cuenta asignada correctamente');
          this.listAccountsCM();
        },
        error: (err) => {
          console.error(err);
          this.showErrorMessage('El uso seleccionado ya ha sido asignado a otra cuenta');
        },
        complete: () => {
        },
      });
    }

    
    private modifyRemove(newAccount: AccountCM){
      this.cmAccountService.modifyServiceAccount(newAccount).subscribe({
        next: (accountCM) => {
          //this.displayLoad = false;
          this.showCreateMessage('Uso removido correctamente');
          this.listAccountsCM();
        },
        error: (err) => {
          console.error(err);
          this.showErrorMessage('No se pudo remover el uso');
        },
        complete: () => {
        },
      });
    }
}
