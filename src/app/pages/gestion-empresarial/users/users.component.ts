import { MessageService } from 'primeng/api';
import { CompanyService } from './../../../services/clients/company.service';
import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/services/utils/authentication.service';
import { Company } from 'src/app/types/clients/Company';
import { UserService } from 'src/app/services/clients/user.service';
import { User } from 'src/app/types/clients/User';
import { FormBuilder, Validators } from '@angular/forms';
import { UserRoleNames } from 'src/app/types/user-roles'
import { UserRoles } from '../../../types/user-roles';

@Component({
    selector: 'app-users',
    templateUrl: './users.component.html',
    styleUrls: ['./users.component.scss'],
})
export class UsersComponent implements OnInit {
    public displayLoad: boolean;
    public isLoading: boolean = false;
    public company: Company = null;
    public representative: User = null;
    public users: User[] = null;
    public userData: User = null;
    public newPassword: string = null;

    public newUserForm = this.formBuilder.group({
        username: ['', [Validators.required]],
        name: ['', [Validators.required]],
        mail: ['', [Validators.required]],
        rol: ['', [Validators.required]],
    });

    roles = [
        { label: "Ingresador", value: "ING" },
        { label: "Aprobador", value: "PRO" }
    ];

    constructor(
        private formBuilder: FormBuilder,
        private messageService: MessageService,
        private companyService: CompanyService,
        private userService: UserService,
        private authService: AuthenticationService
    ) {}

    ngOnInit(): void {

        this.isLoading = true;

        this.companyService.fetchCompanyData(this.getGroupInternalId()).subscribe({
            next: (company) => {
                this.company = company;
            },
            error: (err) => {
                console.error(err);
                this.showFetchError();
            },
            complete: () => {
                this.isLoading = false;
            },
        });

        this.userService.getCompanyRepresentative(this.getGroupInternalId()).subscribe({
            next: (user) => {
                this.representative = user;
            },
            error: (err) => {
                console.error(err);
                this.showFetchError();
            },
            complete: () => {
                this.isLoading = false;
            },
        });

        this.getUsersList();
    }

    private getGroupInternalId(): string{
        const { groupInternalId } = this.authService.authenticatedUser;
        return groupInternalId;
    }

    private getUsersList(){
        this.userService.getCompanyUsers(this.getGroupInternalId()).subscribe({
            next: (userList) => {
                this.users = userList;
                this.users.forEach(user=> {
                    user.roleName = UserRoleNames[user.role];
                });
            },
            error: (err) => {
                console.error(err);
                this.showFetchError();
            },
            complete: () => {
                this.isLoading = false;
            },
        });
    }

    private showFetchError() {
        this.messageService.add({
            key: 'users-toast',
            severity: 'error',
            summary: 'Error',
            detail: 'Error al obtener los datos.',
        });
    }

    private showSuccessMessage(detail: string) {
        this.messageService.add({
            key: 'users-toast',
            severity: 'success',
            summary: 'Listo',
            detail: detail,
        });
    }

    public redirectToNewUser() {
        this.displayLoad = true;
    }

    public newUser() {
        this.newUserForm.markAllAsTouched();

        if (this.newUserForm.invalid) {
            return;
        }

        this.isLoading = true;

        const { username, name, mail, rol } = this.newUserForm.value;

        let user: User = {
            username: username,
            name: name,
            mail: mail,
            role: rol,
            groupInternalId: this.getGroupInternalId(),
        };

        this.userService.create(user).subscribe({
            next: (newUser) => {
                this.newPassword = newUser.password;
                //this.showSuccessMessage(`Usuario ${newUser.username} creado correctamente con la clave ${newUser.password}`);
                this.getUsersList();
            },
            error: (err) => {
                console.error(err);
                this.showFetchError();
            },
            complete: () => {
                this.isLoading = false;
            },
        });
    }

    private blockUser(user: User){
        this.userService.block(user).subscribe({
            next: (blockedUser) => {
                this.showSuccessMessage('Usuario bloqueado correctamente');
                this.displayLoad = false;
                this.getUsersList();
            },
            error: (err) => {
                console.error(err);
                this.showFetchError();
            },
            complete: () => {
                this.isLoading = false;
            },
        });
    }
}
