import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-newuser',
  templateUrl: './new-user.component.html'
})
export class NewUserComponent implements OnInit {
  roles = [
    { label: "Ingresador", value: "ING" },
    { label: "Procesador", value: "PRO" },
    { label: "Representante legal", value: "RPL" },
  ];
  constructor() { }

  ngOnInit(): void {
  }

}
