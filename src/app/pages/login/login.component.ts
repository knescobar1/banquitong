import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { MessageService } from 'primeng/api';

import { AuthenticationService } from 'src/app/services/utils/authentication.service';
import { AppConfig } from '../../config/appconfig';
import { ConfigService } from '../../services/utils/app.config.service';
import { User } from 'src/app/types/clients/User';
import { HttpResponse } from '@angular/common/http';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styles: [
        `
            :host ::ng-deep .pi-eye {
                transform: scale(1.6);
                color: var(--primary-color) !important;
            }

            :host ::ng-deep .pi-eye-slash {
                transform: scale(1.6);
                color: var(--primary-color) !important;
            }
        `,
    ],
})
export class LoginComponent implements OnInit, OnDestroy {
    valCheck: string[] = ['remember'];
    config: AppConfig;
    subscription: Subscription;
    usedApp; string;

    public isLoading: boolean = false;

    public loginForm = this.formBuilder.group({
        username: ['', [Validators.required]],
        password: ['', [Validators.required]],
    });

    constructor(
        public router: Router,
        private formBuilder: FormBuilder,
        // Services
        private authService: AuthenticationService,
        private configService: ConfigService,
        private messageService: MessageService
    ) {}

    ngOnInit(): void {
        this.config = this.configService.config;
        this.subscription = this.configService.configUpdate$.subscribe(
            (config) => {
                this.config = config;
            }
        );
    }

    ngOnDestroy(): void {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }

    public login() {
        this.loginForm.markAllAsTouched();

        if (this.loginForm.invalid) {
            return;
        }

        this.isLoading = true;
        const { username, password } = this.loginForm.value;
        this.authService.loginHeaders({ username, password }).subscribe(resp => localStorage.setItem('token', resp.headers.get('access_token')));
        this.authService.login({ username, password }).subscribe({
            next: (user: User) => {
                this.authService.setToken(user.token);
                this.router.navigateByUrl('');
                this.isLoading = false;
            },
            error: (err) => {
                this.showErrorMessage();
                console.error('Error en el API: ' + err.message);
                this.isLoading = false;
            },
        });
    }

    private showErrorMessage() {
        this.messageService.add({
            key: 'tc',
            severity: 'error',
            summary: 'Error',
            detail: 'El nombre de usuario o contraseña son incorrecto(s).',
        });
    }

    public isInvalidField(fieldName: string): boolean {
        const field = this.loginForm.controls[fieldName];
        return field.touched && field.invalid;
    }

    public tabChange(e) {
        const index = e.index;
        if (index === 0) {
            this.usedApp = 'BUSINESS'
        } else {
            this.usedApp = 'CUSTOMER'
        }
    }
}
