import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { CheckboxModule } from 'primeng/checkbox';
import { PasswordModule } from 'primeng/password';
import { DropdownModule } from 'primeng/dropdown';
import { CalendarModule } from 'primeng/calendar';
import { InputTextModule } from 'primeng/inputtext';
import { TableModule } from 'primeng/table';
import { ToggleButtonModule } from 'primeng/togglebutton';
import { CardModule } from 'primeng/card';
import { DividerModule } from 'primeng/divider';
import { InputTextareaModule } from 'primeng/inputtextarea';


import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { ConsolidatedComponent } from './consolidated/consolidated.component';

import { AccountsComponent } from './gestion-empresarial/accounts/accounts.component';
import { NewUserComponent } from './gestion-empresarial/newuser/new-user.component';

import { CollectionsConfigComponent } from './collections/collections-config/collections-config.component';
import { CollectionsEnableComponent } from './collections/collections-enable/collections-enable.component';
import { CollectionsReferenceidComponent } from './collections/collections-referenceid/collections-referenceid.component';
import { CollectionsProcessComponent } from './collections/collections-process/collections-process.component';
import { CollectionsReportsComponent } from './collections/collections-reports/collections-reports.component';
import { CollectionsListComponent } from './collections/collections-list/collections-list.component';
import { CollectionsNeworderComponent } from './collections/collections-neworder/collections-neworder.component';
import { ButtonModule } from 'primeng/button';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { ToastModule } from 'primeng/toast';
import { MessageService } from 'primeng/api';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { RegisterComponent } from './register/register.component';
import { InputNumberModule } from 'primeng/inputnumber';

import { AccountingBookConfigComponent } from './accounting/accounting-book-config/accounting-book-config.component';
import { AccountingBookReportComponent } from './accounting/accounting-book-report/accounting-book-report.component';

import { PaymentsTransactionsReportComponent } from './payments/payments-transactions-report/payments-transactions-report.component';
import { PaymentsOrdersReportComponent } from './payments/payments-orders-report/payments-orders-report.component';
import { PaymentsAproveComponent } from './payments/payments-aprove/payments-aprove.component';
import { PaymentsRecurrentConfigComponent } from './payments/payments-recurrent-config/payments-recurrent-config.component';
import { PaymentsRecurrentRegisteredComponent } from './payments/payments-recurrent-registered/payments-recurrent-registered.component';
import { PaymentReadyComponent } from './payments/payments-recurrent-config/payment-ready/payment-ready.component';
import { PaymentProgramComponent } from './payments/payments-recurrent-config/payment-program/payment-program.component';
import { PaymentrefLoadorderComponent } from './payments/payments-referenceid/paymentref-loadorder/paymentref-loadorder.component';
import { PaymentBasicAddComponent } from './payments/payments-basic-services/payment-basic-add/payment-basic-add.component';
import { PaymentBasicOrdersComponent } from './payments/payments-basic-services/payment-basic-orders/payment-basic-orders.component';
import { UsersComponent } from './gestion-empresarial/users/users.component';
import { RadioButtonModule } from 'primeng/radiobutton';
import { ToolbarModule } from 'primeng/toolbar';
import { TooltipModule } from 'primeng/tooltip';
import { FileUploadModule } from 'primeng/fileupload';
import { HttpClientModule } from '@angular/common/http';
import { DialogModule } from 'primeng/dialog';

import { NgxSpinnerModule } from 'ngx-spinner';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { TabViewModule } from 'primeng/tabview';
import { PayServiceComponent } from './customer-services/pay-service/pay-service.component';
import { ConfigRecurrentPaymentsComponent } from './customer-services/config-recurrent-payments/config-recurrent-payments.component';
import { PaymentsHistoryComponent } from './customer-services/payments-history/payments-history.component';
import { DeactivateRecurrentPaymentsComponent } from './customer-services/deactivate-recurrent-payments/deactivate-recurrent-payments.component';

@NgModule({
    declarations: [
        HomeComponent,
        LoginComponent,
        ConsolidatedComponent,
        AccountsComponent,
        NewUserComponent,
        CollectionsConfigComponent,
        CollectionsEnableComponent,
        CollectionsReferenceidComponent,
        CollectionsProcessComponent,
        CollectionsReportsComponent,
        CollectionsListComponent,
        CollectionsNeworderComponent,
        RegisterComponent,
        AccountingBookConfigComponent,
        AccountingBookReportComponent,
        PaymentsTransactionsReportComponent,
        PaymentsOrdersReportComponent,
        PaymentsAproveComponent,
        PaymentsRecurrentConfigComponent,
        PaymentsRecurrentRegisteredComponent,
        PaymentReadyComponent,
        PaymentProgramComponent,
        PaymentrefLoadorderComponent,
        PaymentBasicAddComponent,
        PaymentBasicOrdersComponent,
        UsersComponent,
        PayServiceComponent,
        ConfigRecurrentPaymentsComponent,
        PaymentsHistoryComponent,
        DeactivateRecurrentPaymentsComponent
    ],
    imports: [
        ReactiveFormsModule,
        CommonModule,
        FormsModule,
        CalendarModule,
        PasswordModule,
        DropdownModule,
        InputTextModule,
        TableModule,
        CheckboxModule,
        ToggleButtonModule,
        ProgressSpinnerModule,
        CardModule,
        DividerModule,
        ButtonModule,
        ToastModule,
        MessageModule,
        InputNumberModule,
        DropdownModule,
        InputTextareaModule,
        RadioButtonModule,
        ToolbarModule,
        TooltipModule,
        FileUploadModule,
        HttpClientModule,
        DialogModule,
        MessagesModule,
        BrowserAnimationsModule,
        NgxSpinnerModule,
        ToastModule,
        ConfirmDialogModule,
        TabViewModule
    ],
    providers: [MessageService],
})
export class PagesModule { }
