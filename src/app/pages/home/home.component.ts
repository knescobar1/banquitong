import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';

import { AppConfig } from 'src/app/config/appconfig';
import { MainLayoutComponent } from 'src/app/layouts/app.main.component';

@Component({
    templateUrl: './home.component.html',
    styles: ['.logo{width:290px;}']    
    })
export class HomeComponent {

    config: AppConfig;

    subscription: Subscription;

    constructor(public appMain: MainLayoutComponent) {}
}
