import { Component, OnInit } from '@angular/core';
import { MessageService } from 'primeng/api';
import { AccountService } from 'src/app/services/account/account.service';
import { CmAccountService } from 'src/app/services/clients/cm-account.service';
import { AuthenticationService } from 'src/app/services/utils/authentication.service';
import { TransactionCore } from 'src/app/types/accounts/TransactionCore';
import { CustomAccount } from 'src/app/types/clients/CustomAccount';

@Component({
  selector: 'app-consolidated',
  templateUrl: './consolidated.component.html'
})
export class ConsolidatedComponent implements OnInit {

  public accountNumbers: Array<CustomAccount> = [];
  public accountTransactions: TransactionCore[] = null;
  
  selectedAccount: number = null;
  selectedTransaction: string = null;
  selectedSearch: string = null;

  transactionType = [
    { label: "Pagos", value: "PAY" },
    { label: "Cobros", value: "COL" }
  ];

  searchType = [
    { label: "Últimos movimientos", value: "MOV" },
    { label: "Por fechas", value: "DAT" }
  ];

  constructor(      
      private messageService: MessageService,
      private authService: AuthenticationService,
      private coreAccountService: AccountService,
      private cmAccountService: CmAccountService,
      ) { }

  ngOnInit(): void {
    const { groupInternalId } = this.authService.authenticatedUser;
    this.coreAccountService.getAccountsByGroup(groupInternalId).subscribe({
      next: (accountList) => {
        accountList.forEach( account => {
            let coreAccount : CustomAccount  = {
              label: account.accountNumber,
              value: account.accountNumber
            }
            this.accountNumbers.push(coreAccount);
        });
      },
      error: (err) => {
        console.error(err);
        this.showErrorMessage('No se pueden recuperar las cuentas');
      },
      complete: () => {

      },
    });
  }

  public listTransactions(){
    this.cmAccountService.findTransactionsByAccountNumber(this.selectedAccount).subscribe({
      next: (transactionsList) => {
        this.accountTransactions = transactionsList;
      },
      error: (err) => {
        console.error(err);
        this.showErrorMessage('No se pueden recuperar las transacciones, seleccione un campo');
      },
      complete: () => {

      },
    });
  }

  private showErrorMessage(detail: string) {
    this.messageService.add({
      key: 'transactions-toast',
      severity: 'error',
      summary: 'Error',
      detail: detail,
    });
  }


}
