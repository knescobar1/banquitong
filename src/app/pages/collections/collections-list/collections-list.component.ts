import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Observable } from 'rxjs';
import { CollectionService } from 'src/app/services/collections/collection.service';
import { AuthenticationService } from 'src/app/services/utils/authentication.service';
import { Collection } from 'src/app/types/collections/Collection';

@Component({
  selector: 'app-collections-list',
  templateUrl: './collections-list.component.html',
  styles: [
  ]
})
export class CollectionsListComponent implements OnInit {

  getCollections$: Observable<Collection[]>;
  collections: Collection[];
  collectionDialog: boolean;
  collection: Collection;
  labelState: string;

  constructor(
    private collectionService: CollectionService,
    private authService: AuthenticationService,
    private spinner: NgxSpinnerService,
    private router: Router
  ) {
    this.getCollections$ = this.collectionService.obtainCollections(this.authService.authenticatedUser.groupInternalId);
  }
  
  ngOnInit(): void {
    this.getAll();
  }
  
  getAll() {
    this.spinner.show();
    this.getCollections$.subscribe(collections => {
      this.collections = collections;
      this.spinner.hide();
    });
  }

  editar(col: Collection) {
    this.collectionService.setCollection(col);
    this.router.navigate(['collections/referenceid']);
  }

  show(collection: Collection) {
    this.collection = {...collection};
    this.collectionDialog = true;
  }

  hideDialog() {
    this.collectionDialog = false;
  }
  
  debug(data: any) {
    console.log(data);
  }

}
