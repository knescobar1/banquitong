import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ConfirmationService, MessageService } from 'primeng/api';
import { CollectionOrderService } from 'src/app/services/collections/collection-order.service';
import { CollectionService } from 'src/app/services/collections/collection.service';
import { AuthenticationService } from 'src/app/services/utils/authentication.service';
import { Collection } from 'src/app/types/collections/Collection';
import { CollectionOrder } from 'src/app/types/collections/CollectionOrder';

@Component({
  selector: 'app-collections-referenceid',
  templateUrl: './collections-referenceid.component.html',
  providers: [ConfirmationService]
})
export class CollectionsReferenceidComponent implements OnInit {

  collection: Collection;
  uploadedFiles: any[] = [];
  existData: boolean;
  collectionsOrder: CollectionOrder[] = [];
  collectionOrder: CollectionOrder = {};
  submitted: boolean;
  collectionDialog: boolean;
  role: string;
  showProcess: boolean = false;
  showRef: boolean = false;
  upload: File;
  paymentWays: any[];
  selectedPaymentWay: any = {};
  collectionId: string;
  proccessState: string;

  constructor(
    private messageService: MessageService,
    private collectionService: CollectionService,
    private collectionOrderService: CollectionOrderService,
    private confirmationService: ConfirmationService,
    private authService: AuthenticationService,
    private router: Router
  ) {
    this.role = this.authService.authenticatedUser.role;
    this.collectionService.collection$.subscribe((res) => {
      this.collection = res;
      if (this.collection == null) {
        this.back();
      } else {
        this.collectionId = this.collection.internalId;
        this.getAll();
      }
    });

    this.collectionOrder.customer = {};
    this.paymentWays = [
      {
        id: "CSH",
        name: "Efectivo"
      },
      {
        id: "ACC",
        name: "Transferencia"
      }
    ]
  }

  ngOnInit(): void {}

  getAll() {
    this.collectionOrderService.obtainByCollectionId(this.collection.internalId).subscribe({
      next: (res) => {
        if(res) {
          this.collectionsOrder = res;
          this.loadOptions();
        }
      }
    });
  }

  save() {
    this.selectedPaymentWay = {};
    this.collectionOrder = {};
    this.collectionOrder.customer = {};
    this.submitted = false;
    this.collectionDialog = true;
  }

  editCollectionOrder(collect: CollectionOrder) {
    this.collectionOrder = {...collect};
    if(this.collectionOrder.paymentWay === 'ACC') {
      this.selectedPaymentWay = {
        id: 'ACC',
        name: 'Transferencia'
      }
    } else {
      this.selectedPaymentWay = {
        id: 'CSH',
        name: 'Efectivo'
      }
    }
    this.collectionDialog = true;
  }

  deleteCollectionOrder(collect: CollectionOrder) {
    this.confirmationService.confirm({
      message: '¿Estás seguro de eliminar la contrapartida?',
      header: 'Confirmar',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
          this.collectionsOrder = this.collectionsOrder.filter(val => val.journalId !== collect.journalId);
          this.collectionOrder = {};
          this.collectionOrderService.deleteById(collect.internalId).subscribe({
            next: () => {
              this.messageService.add({key: 'gl', severity:'success', summary: 'Completado', detail: 'Eliminado correctamente', life: 3000});
              this.getAll();
            },
            error: () => {
              this.messageService.add({key: 'gl', severity:'error', summary: 'Error', detail: 'Error al eliminar', life: 3000});
            }
          });
          this.hideDialog();
      }
    });
  }

  create() {
    this.submitted = true;

    if(this.collectionOrder.customer.fullName.trim()) {
      this.collectionOrder.collectionId = this.collection.internalId;
      this.collectionOrder.paymentWay = this.selectedPaymentWay.id;
      if(this.collectionOrder.internalId) {
        this.collectionOrderService.updateOne(this.collectionOrder).subscribe({
          next: () => {
            this.messageService.add({key: 'gl', severity:'info', summary:'Éxito', detail:'Orden de Cobro actualizada correctamente'});
            this.getAll();
          },
          error: (err) => {
            console.log('Error al actualizar la Orden de Cobro: ' + err);
            this.messageService.add({key: 'gl', severity:'error', summary:'Error', detail:'Error al actualizar la Orden de Cobro'});
          }
        });
      } else {
        this.collectionOrderService.createOne(this.collectionOrder).subscribe({
          next: () => {
            this.messageService.add({key: 'gl', severity:'info', summary:'Éxito', detail:'Orden de Cobro creada correctamente'});
            this.getAll();
          },
          error: (err) => {
            console.log('Error al crear la Orden de Cobro: ' + err);
            this.messageService.add({key: 'gl', severity:'error', summary:'Error', detail:'Error al crear la Orden de Cobro'});
          }
        });
      }
      this.hideDialog();
      this.selectedPaymentWay = {};
    }
  }

  loadOptions() {
    if (this.collection != null) {
      this.existData = true;
    } else {
      this.existData = false;
    }

    const created = (this.collectionsOrder.filter(ord => ord.processingState !== 'PRO').length > 0);

    if (this.role === 'ING' && (created || this.collectionsOrder.length == 0)) {
      this.showRef = true;
    } else if (this.role === 'PRO' && (created || this.collectionsOrder.length == 0)) {
      this.showProcess = true;
    } else if((this.role === 'ADM' || this.role === 'RPL') && (created || this.collectionsOrder.length == 0)) {
      this.showRef = true;
      this.showProcess = true;
    } else {
      this.showRef = false;
      this.showProcess = false;
    }
  }

  processFile() {
    this.collectionOrderService.uploadToPayCsv(this.collectionId, this.upload).subscribe(
      res => {      
        this.getAll();
        this.messageService.add({ key: 'gl', severity: 'info', summary: 'Éxito', detail: 'Archivo procesado exitosamente' });
      },
      err => {
        console.log(err);        
        this.messageService.add({ key: 'gl', severity: 'error', summary: 'Error', detail: err.message });
      }
    );
  }

  processOrders() {
    this.collectionOrderService.processByCollectionId(this.collectionId).subscribe({
      next: (res) => {
        this.messageService.add({ key: 'gl', severity: 'info', summary: 'Éxito', detail: 'Contrapartidas procesadas correctamente' });
        this.getAll();
        this.loadOptions();
      },
      error: (err) => {
        console.log("Error: " + err);
        this.messageService.add({ key: 'gl', severity: 'error', summary: 'Error', detail: 'Error al procesar las Contrapartidas'});
      }
    });
  }

  hideDialog() {
    this.collectionDialog = false;
    this.submitted = false;
  }

  uploadFile(event: any): void {
    this.upload = event.files[0];
    this.collectionOrderService.validateCSVFile(event.files[0]).subscribe({
      next: () => {
        this.messageService.add({key: 'gl',severity:'success', summary:'Éxito', detail:'Su archivo es válido.'});
      },
      error: (err) => {
        this.upload = undefined;
        this.messageService.add({ key: 'gl', severity: 'error', summary: 'Error', detail: err?.message ?? 'Su archivo no es válido.' });
      }
    });
  }

  back() {
    this.router.navigate(['collections/list']);
  }

}
