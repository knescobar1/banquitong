import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { AccountService } from 'src/app/services/account/account.service';
import { CmAccountService } from 'src/app/services/clients/cm-account.service';
import { CollectionService } from 'src/app/services/collections/collection.service';
import { AuthenticationService } from 'src/app/services/utils/authentication.service';
import { Account } from 'src/app/types/accounts/Account';
import { AccountCM } from 'src/app/types/clients/AccountCM';
import { CollectionChannel, CollectionPeriodicity, CreateCollection } from 'src/app/types/collections/Collection';
import { CollectionServiceOffered } from 'src/app/types/collections/CollectionServiceOffered';

const SERVICES: CollectionServiceOffered[] = [
  {
    name: 'Reparación de dispositivos móviles',
    description: 'Reparación de dispositivos móviles'
  },
  {
    name: 'Alícuota',
    description: 'Alícuota'
  },
  {
    name: 'Matricula Vehicular',
    description: 'Matricula Vehicular'
  }
]

const CHANNEL: CollectionChannel[] = [
  {
    id: 'WEB',
    name: 'Banca Web'
  }
];

const PERIODICITIES: CollectionPeriodicity[] = [
  {
    id: 'MONTH',
    name: 'Mensual'
  },
  {
    id: 'YEAR',
    name: 'Anual'
  }
]

@Component({
  selector: 'app-collections-config',
  templateUrl: './collections-config.component.html',
  styles: [
  ]
})

export class CollectionsConfigComponent implements OnInit {

  groupInternalId: string;
  accounts: AccountCM[] = [];
  account: AccountCM;
  collection: CreateCollection = {};
  services: CollectionServiceOffered[];
  channels: CollectionChannel[];
  periodicities: CollectionPeriodicity[];
  service: CollectionServiceOffered = {};
  startDate: Date;
  endDate: Date;

  constructor(
    private authService: AuthenticationService,
    private accountService: AccountService,
    private messageService: MessageService,
    private collectionService: CollectionService,
    private router: Router,
    private cmAccountService: CmAccountService
    ) {
    this.groupInternalId = this.authService.authenticatedUser.groupInternalId;
    this.services = SERVICES;
    this.channels = CHANNEL;
    this.periodicities = PERIODICITIES;
  }

  ngOnInit(): void {

    this.cmAccountService.getAccountsByGroupIdAndType(this.groupInternalId, 'COL').subscribe({
      next: (accounts) => {
        this.accounts = accounts;
      },
      error: (err) => {
        console.log('Error al obtener las cuentas: ' + err);
        this.messageService.add({key: 'gl', severity:'error', summary:'Error', detail:'No existen cuentas configuradas'});
      },
      complete: () => {}
    });

    // this.accountService.getAccountsByGroup(this.groupInternalId).subscribe({
    //   next: (accounts) => {
    //     this.accounts = accounts;
    //   },
    //   error: (err) => {
    //     console.log('Error al obtener las cuentas: ' + err);
    //     this.messageService.add({key: 'gl', sticky: true, severity:'error', summary:'Error', detail:'No existen cuentas configuradas'});
    //   },
    //   complete: () => {}
    // });

  }

  save() {
    this.collection.serviceOffered = this.service;
    this.formatDate();
    if(this.collection.startCollectionDate < this.collection.endCollectionDate) {
      this.collection.groupInternalId = this.groupInternalId;
      this.collectionService.createCollection(this.collection).subscribe({
        next: () => {
          this.messageService.add({key: 'gl',severity:'success', summary:'Éxito', detail:'Configuración guardada'});
          setTimeout(() => {
            this.router.navigate(['collections/enable']);
          }, 2000);
        },
        error: (err) => {
          console.log('Error al crear la Configuración de Cobro: ' + err);
          this.messageService.add({key: 'gl', severity:'error', summary:'Error', detail:'Error al crear la Configuración de Cobro'});
        },
        complete: () => {}
      });
    } else {
      this.messageService.add({key: 'gl', severity:'error', summary:'Error', detail:'El rango de fechas es incorrecto'});
    }

  }

  formatDate() {
    this.collection.startCollectionDate = this.startDate.toISOString();
    this.collection.endCollectionDate = this.endDate.toISOString();
  }

}
