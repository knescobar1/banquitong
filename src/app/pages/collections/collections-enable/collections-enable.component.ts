import { Component, OnInit } from '@angular/core';

import { NgxSpinnerService } from "ngx-spinner";
import { MessageService } from 'primeng/api';
import { Observable } from 'rxjs';
import { ACTIVE_COLLECTION } from 'src/app/constants/collection-state';

import { CollectionService } from 'src/app/services/collections/collection.service';
import { AuthenticationService } from 'src/app/services/utils/authentication.service';
import { CheckCollection, Collection } from 'src/app/types/collections/Collection';

@Component({
  selector: 'app-collections-enable',
  templateUrl: './collections-enable.component.html'
})
export class CollectionsEnableComponent implements OnInit {

  getCollections$: Observable<Collection[]>;
  collections: Collection[];
  collectionsCheck: CheckCollection[];

  constructor(
    public collectionsService: CollectionService,
    private spinner: NgxSpinnerService,
    private messageService: MessageService,
    private authService: AuthenticationService     
    ) {
      // this.getCollections$ = this.collectionsService.obtainCollections(this.authService.authenticatedUser.groupInternalId);
      this.getCollections$ = this.collectionsService.obtainCollectionsActiveAndInactive(this.authService.authenticatedUser.groupInternalId);
  }

  ngOnInit(): void {
    this.getAll();
  }

  getAll() {
    this.spinner.show();
    this.getCollections$.subscribe(collections => {
      console.log(collections);
      
      this.collections = collections;
      this.collectionsCheck = collections;
      this.collectionsCheck.forEach(col => {
        col.stateCheck = (col.state === ACTIVE_COLLECTION) ? true : false;
      });
      this.spinner.hide();
    });
  }

  check(col: CheckCollection): void {
    const {stateCheck, ...collect} = col;
    this.collectionsService.setStateCollection(collect).subscribe({
      next: () => {
        this.messageService.add({key:'gl', severity: 'success', summary: 'Éxito', detail: 'Estado cambiado correctamente'});
        setTimeout(() => {
          this.getAll();
        }, 1000);
      },
      error: (err) => {
        console.log(err);
        this.messageService.add({key:'gl', severity: 'error', summary: 'Error', detail: 'Error al cambiar de estado'});
      }
    });
  }
}
