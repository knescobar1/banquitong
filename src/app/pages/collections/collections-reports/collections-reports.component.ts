import { Component, OnInit } from '@angular/core';
import { CollectionOrder } from '../../../types/collections/CollectionOrder';
import { CollectionOrderService } from '../../../services/collections/collection-order.service';
import { MessageService } from 'primeng/api';
import { CollectionService } from 'src/app/services/collections/collection.service';
import { Observable } from 'rxjs';
import { AuthenticationService } from 'src/app/services/utils/authentication.service';
import { Collection } from 'src/app/types/collections/Collection';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
	selector: 'app-collections-reports',
	templateUrl: './collections-reports.component.html',
	styles: [`:host ::ng-deep .p-multiselect {
		min-width: 15rem;
	}

	:host ::ng-deep .multiselect-custom-virtual-scroll .p-multiselect {
		min-width: 20rem;
	}

	:host ::ng-deep .multiselect-custom .p-multiselect-label,  {
		padding-top: 0.75rem;
		padding-bottom: 0.75rem;

	}

	:host ::ng-deep .multiselect-custom .country-item.country-item-value {
		padding: .25rem .5rem;
		border-radius: 3px;
		display: inline-flex;
		margin-right: .5rem;
		background-color: var(--primary-color);
		color: var(--primary-color-text);
	}

	:host ::ng-deep .multiselect-custom .country-item.country-item-value img.flag {
		width: 17px;
	}

	:host ::ng-deep .multiselect-custom .country-item {
		display: flex;
		align-items: center;
	}

	:host ::ng-deep .multiselect-custom .country-item img.flag {
		width: 18px;
		margin-right: .5rem;
	}

	:host ::ng-deep .multiselect-custom .country-placeholder {
		padding: 0.25rem;
	}

    `]
})
export class CollectionsReportsComponent implements OnInit {

	obtainCollections$: Observable<Collection[]>;

	selectedType: string;
	collectionOrders: CollectionOrder[];
	collections: Collection[];
	toogleReport: boolean = false;
	status: string[] = [];

	services: string[];
	selectedService: string = "";

	initDate: Date;
	finishDate: Date;
	cols: any;
	nameDocument = 'Reposrte-Cobros';

	constructor(private messageService: MessageService,
		private collectionOrderService: CollectionOrderService,
		private collectionService: CollectionService,
		private authService: AuthenticationService,
		private spinner: NgxSpinnerService)
	{
		this.obtainCollections$ = this.collectionService.obtainCollections(authService.authenticatedUser.groupInternalId);
		this.cols = [
			{ field: 'paymentWay', header: 'Forma de pago'},
			{ field: 'debtorAccount', header: 'Cuenta Cliente' },
			{ field: 'customer.customerId', header: 'Identificacion cliente' },
			{ field: 'customer.fullName', header: 'Nombre Cliente' },
			{ field: 'referenceId', header: 'Contrapartida' },
			{ field: 'amount', header: 'Valor enviado' },
			{ field: 'collectedDate', header: 'Fecha' },
			{ field: 'state', header: 'Estado' },
			{ field: 'reference', header: 'Referencia' }
		];
	}

	ngOnInit(): void {
		this.spinner.show();

		this.obtainCollections$.subscribe(
			col => {
				this.collections = col;
				this.services = this.collections.map(tempCol => tempCol.serviceOffered.name);
				this.selectedService = this.services[0];
				this.spinner.hide();
			}, err => {
				this.messageService.add({
					severity: 'error',
					summary: 'Error',
					detail: err?.message ?? 'Algo ha salido mal'
				});
			});
	}

	query(): void {
		this.collectionOrderService.reportCollection(this.selectedService, this.status,
			this.initDate, this.finishDate).subscribe(
				collectionOrder => {
					this.toogleReport = true;
					this.collectionOrders = collectionOrder;
				},
				err => {
					this.messageService.add({
						severity: 'error',
						summary: 'Error',
						detail: err?.message ?? 'Algo ha malido sal'
					});
				});
	}

}
