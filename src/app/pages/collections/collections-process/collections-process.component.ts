import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { MessageService } from 'primeng/api';
import { AuthenticationService } from 'src/app/services/utils/authentication.service';
import { CollectionOrderService } from '../../../services/collections/collection-order.service';
import { CollectionOrder } from '../../../types/collections/CollectionOrder';

@Component({
  selector: 'app-collections-process',
  templateUrl: './collections-process.component.html',
  styles: [
  ]
})
export class CollectionsProcessComponent {

  constructor(private collectionOrderService: CollectionOrderService,
    private messageService: MessageService,
    private spinner: NgxSpinnerService,
    private authService: AuthenticationService) {
    this.periodicity = [
      'Mensual',
      'Anual'
    ];
    this.paymentWay = [
      'ACC',
      'CSH'
    ];
    this.selectedPeriodicity = this.periodicity[0];
    this.selectedPaymentWay = this.paymentWay[0];
  }

  referenceId: string;
  serviceName: string;
  toogleReport: boolean = false;
  paymentWay: string[];
  selectedPaymentWay: string = "";
  periodicity: string[];
  selectedPeriodicity: string = "";
  collectionOrders: CollectionOrder[];
  data: any[] = [
    {
      reference: "1",
      service: "2",
      channel: "Activo",
      periodic: "04/04/2020",
      account: "05/04/2020",
    }
  ];

  ngOnInit(): void {
  }

  query(): void {
    this.collectionOrderService.obtainByCollectionServiceName(
      this.serviceName, this.referenceId).subscribe(
        collectionOrder => {
          this.toogleReport = true;
          this.collectionOrders = collectionOrder;
        },
        err => {
          this.messageService.add({
            severity: 'error',
            summary: 'Error',
            detail: err?.message ?? 'Algo ha salido mal'
          });
        });
  };

  approve(collect: CollectionOrder): void {
    this.collectionOrderService.payCollectionOrder(
      collect.internalId, this.selectedPaymentWay);
  }

}
