import { CollectionServiceOffered } from './CollectionServiceOffered';

export type Collection = {
    internalId: string,
    groupInternalId: string,
    creationDate: string,
    serviceOffered: CollectionServiceOffered,
    state: string,
    periodicity: string,
    startCollectionDate: string,
    endCollectionDate: string,
    channel?: string,
    creditorAccount?: number,
    reference?: string,
    records?: number,
    paidRecords?: number,
    failedRecords?: number,
    totalCollectionValue?: number,
    failedValue?: number,
};

export type CreateCollection = {
    groupInternalId?: string,
    creditorAccount?: number,
    creationDate?: Date,
    serviceOffered?: CollectionServiceOffered,
    state?: string,
    periodicity?: string,
    startCollectionDate?: string,
    endCollectionDate?: string,
    channel?: string,
    reference?: string
}

export type CheckCollection = {
    internalId: string,
    groupInternalId: string,
    creationDate: string,
    serviceOffered: CollectionServiceOffered,
    state: string,
    periodicity: string,
    startCollectionDate: string,
    endCollectionDate: string,
    stateCheck?: boolean;
}

export type CollectionChannel = {
    id: string,
    name: string
}

export type CollectionPeriodicity = {
    id: string,
    name: string
}