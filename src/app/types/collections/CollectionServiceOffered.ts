export type CollectionServiceOffered = {
    name?: string,
    description?: string;
}