import { CollectionCustomer } from './CollectionCustomer';

export type CollectionOrder = {
  collectionId?: string;
  referenceId?: string;
  customer?: CollectionCustomer;
  debtorAccount?: number;
  paymentWay?: string;
  amount?: number;
  reference?: string;
  internalId?: string;
  state?: string;
  collectedDate?: Date;
  startCollectionDate?: Date;
  endCollectionDate?: Date;
  journalId?: string;
  processingState?: string;
};
