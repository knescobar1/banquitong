export type Payment = {
    internalId: string,
    groupInternalId: string,
    serviceName: string,
    serviceDescription: string,
    state: string,
    debtorAccount: number,
    processDate: string,
    records: number,
    paidRecords: number;
    failedRecords: number;
    totalPaymentValue: number;
    paidValue: number;
    failedValue: number;
    journalId: string;
};