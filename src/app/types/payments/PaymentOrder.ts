export type PaymentOrder = {
    internalId?: string,
    referenceId?: string,
    paymentId?: string,
    creditorIdentification?: string,
    creditorIdentificationType?: string,
    creditorName?: string,
    creditorAccount: number,
    creditorAccountType?: string,
    creditorEmail?: string,
    amount: number,
    state?: string,
    description?: string,
    paymentDate?: string
};