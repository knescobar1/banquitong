export type ServiceOffered = {
    internalId: string,
    groupInternalId: string,
    referenceId: string,
    creditorAccount: number,
    creditorIdentification: string,
    creditorEmail: string,
    debtorAccount: number,
    serviceName: string,
    description: string,
    maxAmount: number,
    aprobationState: string,
    recurrentState: string, //
    frecuency: string, //
    firstScheduleDate: string, //
    finalScheduleDate: string, //
    lastPaymentDate: string,
};