export type Transaction = {
    creditorGroupInternalId: string,
    creditorAccountNumber: number,
    debtorGroupInternalId: string,
    debtorAccountNumber: number,
    serviceLevel: string,
    amount: number,
    state: string,
    channel: string,
    externalOperation: string,
    reference: string,
    documentNumber: string,
    transactionNumber: string,
};