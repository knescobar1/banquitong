export type ServicePayment = {
    internalId: string,
    serviceId: string,
    state: string,
    charge: number,
    amount: number,
    journalId: string,
};