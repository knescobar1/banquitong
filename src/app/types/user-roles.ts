export enum UserRoles {
  ADMINISTRADOR = 'ADM',
  REPRESENTANTE_LEGAL = 'RPL',
  INGRESADOR = 'ING',
  APROBADOR = 'PRO',
  CUSTOMER = 'CST'
}

export const UserRoleNames: Record<UserRoles, string> = {
  ADM: 'Administrador',
  RPL: 'Representante legal',
  ING: 'Ingresador',
  PRO: 'Aprobador',
  CST: 'Cliente'
};
