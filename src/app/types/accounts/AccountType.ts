export type AccountType = {
    name: string,
    accrual: string,
    term: string,
    notes: string
}
