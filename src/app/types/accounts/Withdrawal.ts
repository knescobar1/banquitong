export type Withdrawal = {
    accountNumber: number,
    amount: number,
    notes: string,
    transactionChannel: string,
    documentNumber: string,
    transactionNumber: string
}
