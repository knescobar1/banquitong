export type TransactionCore = {
    creditorGroupInternalId: string,
    creditorAccountNumber: number,
    debtorGroupInternalId: string,
    debtorAccountNumber: string,
    creationDate: string,
    serviceLevel: string,
    amount: number,
    state: string, 
    channel: string,
    externalOperation: string,
    reference: string,
    documentNumber: string,
    transactionNumber: string
};