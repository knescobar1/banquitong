export type Account = {
    accountNumber: number,
    familyId: string,
    customerGroupId: string,
    accountType: string,
    openingDate: Date,
    maturityDate: Date,
    balance: number,
    state: string
};
