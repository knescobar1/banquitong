export type Deposit = {
    referenceId: string,
    accountNumber: number,
    amount: number,
    notes: string,
    transactionChannel: string,
    documentNumber: string,
    transactionNumber: string
}
