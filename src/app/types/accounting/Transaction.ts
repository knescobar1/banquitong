export type Transaction = {
    transactionReference: string,
    amount: number,
    type: string,
    transactionDate: Date
}
