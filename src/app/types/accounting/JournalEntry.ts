import { JournalEntryDetail } from "./JorunalEntryDetail";

export type JournalEntry = {
    groupInternalId: string,
    transactionReference: string,
    accountingDate: Date,
    transactionDate: Date,
    memo: string,
    journalEntries: JournalEntryDetail[];
}