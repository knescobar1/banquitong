export type GeneralLedgerAccount = {
    name?: string,
    type?: string,
    description?: string,
    code?: string,
    currentBalance?: number,
    openingBalance?: number
}
