export type JournalEntryDetail = {
    ledgerAccount: string,
    debit: number,
    credit: number
}