export type GeneralLedgerReport = {
    accountCode: string,
    accountingDate: Date,
    memo: string,
    debit: number,
    credit: number,
    accountBalance: number
}