import { UserRoles } from '../user-roles';

export type User = {
  username: string;
  password?: string;
  name?: string;
  mail?: string;
  role?: UserRoles;
  state?: string;
  groupInternalId?: string;
  token?: string;
  roleName? :string;
  app?: string;
};
