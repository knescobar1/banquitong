export interface CreateCompany {
  groupInternalId: string;
  companyRuc: string;
}
