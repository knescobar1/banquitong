export type AccountCM = {
    id?: string,
    accountNumber?: number,
    accountType?: string,
    serviceType?: string,
    accountingBalance?: number,
    availableBalance?: number,
    accountState?: string,
    groupInternalId?: string
};
