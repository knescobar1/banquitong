export interface Group {
  internalId: string;
  documentType: string;
  documentId: string;
  name: string;
  email: string;
  segment: Segment;
  assignedBranch: string;
  phones: Phone[];
  addresses: Address[];
  error?: any; // Llega en caso de error
}

export interface Address {
  type: string;
  country: string;
  region: string;
  city: string;
  line1: string;
  line2: string;
  postalCode: string;
  primary: boolean;
}

export interface Phone {
  type: string;
  phone: string;
  primary: boolean;
}

export interface Segment {
  code: string;
  name: string;
}
