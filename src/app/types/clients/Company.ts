export type Company = {
  companyRuc: string;
  companyName: string;
  phoneReference: string;
  businessName: string;
  groupInternalId?: string;
  error?: any; // objeto que llega en caso de error
};
