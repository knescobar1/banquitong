/**
 * El aprobador solo tiene acceso a las rutas que contengan
 * la palabra 'apro' o 'enable'. Esta función puede ser usada
 * tanto por el aprobador como por el ingresador ya que sus
 * permisos son el inverso de los permisos del otro.
 * @param path ruta a verificar
 */
export function matchesAproverPath(path: string): boolean {
  const isAproverRegex: RegExp = /apro|enable/gim;
  return isAproverRegex.test(path);
}

/**
 * Tanto el Ingresador como el aprobador tienen acceso a los
 * reportes
 * @param path ruta a verificar.
 */
export function matchesReportPath(path: string): boolean {
  const isReportPath: RegExp = /report/gim;
  return isReportPath.test(path);
}

/**
 * El aprobador no tiene accesos a configs.
 * @param path ruta a verificar.
 */
export function matchesConfigPath(path: string): boolean {
  const isConfigPath: RegExp = /config/gim;
  return isConfigPath.test(path);
}

/**
 * Esta función es usada para que el Representante legal no
 * tenga accesso a contabilidad.
 * @param path ruta a verificar
 */
export function matchesAccountingPath(path: string): boolean {
  const isAccountingPath: RegExp = /accounting/gim;
  return isAccountingPath.test(path);
}
