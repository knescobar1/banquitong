import {
  AbstractControl,
  AsyncValidatorFn,
  ValidationErrors,
} from '@angular/forms';
import { Observable, of, tap } from 'rxjs';
import { catchError, debounceTime, finalize, map } from 'rxjs/operators';

import { Company } from 'src/app/types/clients/Company';
import { Group } from 'src/app/types/clients/Group';
import { CompanyService } from '../services/clients/company.service';
import { GroupService } from '../services/clients/group.service';

export class RegisterCompanyValidator {
  public static alreadyInCmClientsValidator(
    companyService: CompanyService
  ): AsyncValidatorFn {
    return (control: AbstractControl): Observable<ValidationErrors> => {
      if (control.value.length !== 13) return null;

      companyService.isLoading = true;

      return companyService.fetchCompanyByRUC(control.value).pipe(
        debounceTime(500),
        map(this.handleCompanyResponse),
        catchError((e) => of(this.handleCompanyResponse(e))),
        finalize(() => (companyService.isLoading = false))
      );
    };
  }

  private static handleCompanyResponse(
    company: Company
  ): { [k: string]: boolean } | null {
    const companyIsPresentInCmClients: boolean = company?.error?.status !== 404;

    return companyIsPresentInCmClients
      ? { companyAlreadyInCmClients: true }
      : null;
  }

  public static groupInCoreCustomersValidator(
    groupService: GroupService
  ): AsyncValidatorFn {
    return (control: AbstractControl): Observable<ValidationErrors> => {
      if (control.value.length !== 13) return null;

      groupService.isLoading = true;

      return groupService.fetchGroupByRUC(control.value).pipe(
        debounceTime(500),
        tap((result: Group) => {
          if (result?.name !== undefined) {
            groupService.group = result;
          }
        }),
        map(this.handleGroupResponse),
        catchError((e) => of(this.handleGroupResponse(e))),
        finalize(() => (groupService.isLoading = false))
      );
    };
  }

  private static handleGroupResponse(
    group: Group
  ): { [k: string]: boolean } | null {
    const groupIsNotInCoreCustomers: boolean = group?.error?.status === 404;

    return groupIsNotInCoreCustomers
      ? { groupIsNotPresentInCoreCustomers: true }
      : null;
  }
}
