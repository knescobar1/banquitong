import { Component } from '@angular/core';
import { PrimeNGConfig } from 'primeng/api';

import { ConfigService } from './services/utils/app.config.service';

@Component({
    selector: 'app-root',
    template: `<router-outlet></router-outlet>`
})
export class AppComponent {

    menuMode = 'static';

    constructor(
        private primengConfig: PrimeNGConfig,
        public configService: ConfigService,
    ) { }

    ngOnInit() {
        this.primengConfig.ripple = true;
        document.documentElement.style.fontSize = '14px';
    }
}
