import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';

import { MenuService } from './services/utils/app.menu.service';
import { ConfigService } from './services/utils/app.config.service';
import { PagesModule } from './pages/pages.module';
import { MainLayoutComponent } from './layouts/app.main.component';
import { ComponentsModule } from './components/components.module';
import { CookieService } from 'ngx-cookie-service';

import { NgxSpinnerModule } from "ngx-spinner";
import { ToastModule } from 'primeng/toast';

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        AppRoutingModule,
        HttpClientModule,
        BrowserAnimationsModule,
        ComponentsModule,
        PagesModule,
        NgxSpinnerModule,
        ToastModule
    ],
    declarations: [
        AppComponent,
        MainLayoutComponent
    ],
    providers: [
        {provide: LocationStrategy, useClass: HashLocationStrategy},
        MenuService, ConfigService, CookieService
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
