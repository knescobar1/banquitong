export class Register {
    documentId?: string;
    accountNumber?: string;
    typeId?: string;
    familyId?: string;
    customerGroupId?: string;
}
