import { Injectable } from '@angular/core';
import {environment} from "../../../environments/environment";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Withdrawal} from "../../types/accounts/Withdrawal";
import {Observable} from "rxjs";
import {Deposit} from "../../types/accounts/Deposit";
import {TransactionCore} from "../../types/accounts/TransactionCore";

const URL = environment.coreAccounntsUrl;
const TRANSACTION = `${URL}/transaction`

@Injectable({
  providedIn: 'root'
})
export class TransactionService {

  constructor(private http: HttpClient) {}

  public getByAccountNumber(account: number): Observable<TransactionCore> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
    });
    return this.http.post<TransactionCore>(`${TRANSACTION}`, account, {headers});
  }

  public makeWithdrawal(withdrawal: Withdrawal): Observable<Withdrawal> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
    });
    return this.http.post<Withdrawal>(`${TRANSACTION}/withdrawal`, withdrawal, {headers});
  }

  public makeDeposit(deposit: Deposit): Observable<Deposit> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
    });
    return this.http.post<Deposit>(`${TRANSACTION}/deposit`, deposit, {headers});
  }

  public makeTransaction(transaction: TransactionCore): Observable<TransactionCore> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
    });
    return this.http.post<TransactionCore>(`${TRANSACTION}/transfer`, transaction, {headers});
  }
}
