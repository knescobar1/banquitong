import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AccountTypesEnum } from 'src/app/models/account-types.enum';
import { environment } from '../../../environments/environment';
import { Account } from '../../types/accounts/Account';

const URL = environment.coreAccounntsUrl;
const ACCOUNT = `${URL}/accounts`;

@Injectable({
  providedIn: 'root',
})
export class AccountService {

  constructor(private http: HttpClient) {}

  public fetchGroupAccount(searchParams: {
    groupId: string;
    accountType: AccountTypesEnum;
    accountNumber: string;
  }): Observable<Account> {

    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
    });

    const { groupId, accountType, accountNumber } = searchParams;

    const params = new HttpParams()
      .set('groupId', groupId)
      .set('type', accountType)
      .set('number', accountNumber);

    return this.http.get<Account>(`${ACCOUNT}/search`, { params, headers });
  }

  public getAccountByNumber(num: number): Observable<Account> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
    });
    return this.http.get<Account>(`${ACCOUNT}/${num}`, {headers});
  }

  public create(account: Account): Observable<Account> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
    });
    return this.http.post<Account>(`${ACCOUNT}`, account, {headers});
  }

  public activateById(id: string): Observable<Account> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
    });
    return this.http.get<Account>(`${ACCOUNT}/activate/${id}`, {headers});
  }

  public disableById(id: string): Observable<Account> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
    });
    return this.http.get<Account>(`${ACCOUNT}/disable/${id}`, {headers});
  }

  public getByGroupId(group: string): Observable<Account> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
    });
    return this.http.get<Account>(`${ACCOUNT}/${group}`, {headers});
  }

  public getAccountsByGroup(group: string): Observable<Account[]> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
    });
    return this.http.get<Account[]>(`${ACCOUNT}/group/${group}`, {headers});
  }

  public obtainActivesByGroup(group: string): Observable<Account[]> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
    });
    return this.http.get<Account[]>(`${ACCOUNT}/group/active/${group}`, {headers});
  }
}
