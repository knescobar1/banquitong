import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, tap } from 'rxjs';
import { User } from 'src/app/types/clients/User';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root',
})
export class UserService {
    private USER_URL = `${environment.cmClientsUrl}/user`;
    public userRepresentative: User | null = null;

    constructor(private http: HttpClient) {}

    public getCompanyRepresentative(id: string): Observable<User> {
        const endpointURL: string = `${this.USER_URL}/rpl/${id}`;
        return this.http.get<User>(endpointURL).pipe(
            tap((user) => (this.userRepresentative = user))
        );
    }

    public getCompanyUsers(id: string): Observable<User[]> {
        const endpointURL: string = `${this.USER_URL}/company/${id}`;
        return this.http.get<User[]>(endpointURL);
    }

    public create(user: User): Observable<User> {
        return this.http.post<User>(`${this.USER_URL}`,user);
    }

    public block(user: User): Observable<User> {
        return this.http.put<User>(`${this.USER_URL}/block`,user);
    }
}
