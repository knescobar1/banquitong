import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { TransactionCore } from 'src/app/types/accounts/TransactionCore';
import { environment } from 'src/environments/environment';
import { AccountCM } from '../../types/clients/AccountCM';

@Injectable({
  providedIn: 'root'
})
export class CmAccountService {
  private CMACCOUNTS_URL = `${environment.cmClientsUrl}/account`;

  constructor(private http: HttpClient) { }

  public findAccountByNumber(account: AccountCM){
    return this.http.get<AccountCM>(`${this.CMACCOUNTS_URL}/${account.accountNumber}`);
  }

  public registerAccountOnCm(account: AccountCM): Observable<AccountCM> {
    return this.http.post<AccountCM>(`${this.CMACCOUNTS_URL}`, account);
  }

  public modifyAccount(account: AccountCM): Observable<AccountCM> {
    return this.http.put<AccountCM>(`${this.CMACCOUNTS_URL}`, account);
  }

  public modifyServiceAccount(account: AccountCM): Observable<AccountCM> {
    return this.http.put<AccountCM>(`${this.CMACCOUNTS_URL}/service`, account);
  }

  public listAccountsByGroupInternalId(groupInternalId: string) {
    return this.http.get<AccountCM[]>(`${this.CMACCOUNTS_URL}/listAccountsByGroupInternalId/${groupInternalId}`);
  }

  public findTransactionsByAccountNumber(accountNumber: number): Observable<TransactionCore[]> {
    return this.http.get<TransactionCore[]>(`${this.CMACCOUNTS_URL}/${accountNumber}/transactions`);
  }

  public getAccountsByGroupIdAndType(groupId: string, serviceType: string): Observable<AccountCM[]> {
    return this.http.get<AccountCM[]>(`${this.CMACCOUNTS_URL}/listAccountsByGroupInternalIdAndServiceType/${groupId}/${serviceType}`);
  }

}
