import { environment } from 'src/environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { Group } from 'src/app/types/clients/Group';

@Injectable({
  providedIn: 'root',
})
export class GroupService {
  private groupsURL: string = `${environment.coreCustomersUrl}/groups`;

  public isLoading: boolean = false;
  private _group = new BehaviorSubject<Group | null>(null);

  public get group(): Group | null {
    return this._group.value;
  }

  public set group(newGroup: Group | null) {
    this._group.next(newGroup);
  }

  public get group$() {
    return this._group.asObservable();
  }

  constructor(private http: HttpClient) {}

  public fetchGroupByRUC(ruc: string): Observable<Group> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
    });
    const url: string = `${this.groupsURL}/ruc/${ruc}`;
    return this.http.get<Group>(url,{headers});
  }
}
