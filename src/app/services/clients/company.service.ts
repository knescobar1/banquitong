import { CreateCompany } from './../../types/clients/CreateCompany';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, tap } from 'rxjs';
import { Company } from 'src/app/types/clients/Company';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class CompanyService {
  private companyURL = `${environment.cmClientsUrl}/company`;

  public isLoading: boolean = false;
  public userCompany: Company | null = null;

  constructor(private http: HttpClient) {}

  public createCompany(data: CreateCompany): Observable<CreateCompany> {
    return this.http.post<CreateCompany>(this.companyURL, data);
  }

  public fetchCompanyData(id: string): Observable<Company> {
    const endpointURL: string = `${this.companyURL}/id/${id}`;

    return this.http.get<Company>(endpointURL).pipe(
      tap((company) => (this.userCompany = company)),
      tap(this.saveCompanyDataToLocalStorage)
    );
  }

  private saveCompanyDataToLocalStorage(company: Company): void {
    if (company === null) return;
    localStorage.setItem('companyData', JSON.stringify(company));
  }

  public fetchCompanyByRUC(ruc: string) {
    const url: string = `${this.companyURL}/ruc/${ruc}`;
    return this.http.get<Company>(url);
  }
}
