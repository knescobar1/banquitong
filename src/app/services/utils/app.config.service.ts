import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { AppConfig } from '../../config/appconfig';

@Injectable()
export class ConfigService {

    config: AppConfig;

    constructor() {
        const tempConfig: AppConfig = JSON.parse(localStorage.getItem('config'));
        const themeElement = document.getElementById('theme-css');
        document.documentElement.style.fontSize = 10 + 'px';
        if(tempConfig) {
            themeElement.setAttribute('href', `assets/theme/${tempConfig.theme}/theme.css`);
            this.config = tempConfig;
        } else {
            this.config = {
                theme: 'md-light-indigo',
                dark: false,
                inputStyle: 'outlined',
                ripple: true
            }
        }
    }

    private configUpdate = new Subject<AppConfig>();

    configUpdate$ = this.configUpdate.asObservable();

    updateConfig(config: AppConfig) {
        this.config = config;
        this.configUpdate.next(config);
    }

    getConfig() {
        return this.config;
    }
}
