import { Injectable } from '@angular/core';
import {
    ActivatedRouteSnapshot,
    Router,
    RouterStateSnapshot,
} from '@angular/router';
import { AuthenticationService } from './authentication.service';

@Injectable({
    providedIn: 'root',
})
export class AuthGuardService {
    constructor(
        private router: Router,
        private authService: AuthenticationService
    ) {}

    // NOTE: Esto va a cambiar a JWT en un futuro.
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const user = this.authService.getUserDataFromLocalStorage();

        const isUserInLocalStorage: boolean = user !== null;
        if (isUserInLocalStorage) {
            this.authService.authenticatedUser = user;
            return true;
        } else {
            this.router.navigate(['pages/login']);
            return false;
        }
    }
}
