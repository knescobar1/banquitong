import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Observable, tap } from 'rxjs';
import { Company } from '../../types/clients/Company';
import { Login } from '../../models/login.model';
import { User } from '../../types/clients/User';
import { environment } from 'src/environments/environment';
import { Register } from 'src/app/models/company.model';
import { UserRoleNames, UserRoles } from 'src/app/types/user-roles';

const LOGIN = `${environment.cmClientsUrl}`;
const USERS_URL = `${environment.cmClientsUrl}/user`;
const COMPANY_URL = `${environment.cmClientsUrl}/company`;

@Injectable({
    providedIn: 'root',
})
export class AuthenticationService {
    public authenticatedUser: User | null;

    constructor(private http: HttpClient, private cookie: CookieService) {}

    public login(user: Login): Observable<User> {
        return this.http.put<User>(`${USERS_URL}/login`, user).pipe(
            tap((user) => {
                this.authenticatedUser = user;
            }),
            tap(this.saveUserDataToLocalStorage)
        );
    }

    public loginHeaders(user: Login) {
        const params: HttpParams = new HttpParams()
            .set("username", user.username)
            .set("password", user.password);
        return this.http.post<HttpResponse<Login>>(`${LOGIN}/login`, {}, { params, observe: 'response', withCredentials: true });
    }

    public setToken(token: string) {
        this.cookie.set('token', token);
    }

    public getToken() {
        return this.cookie.get('token');
    }

    public close() {
        this.cookie.deleteAll();
        this.removeUserDataFromLocalStorage();
    }

    public isUserLoggedIn() {
        const userToken = this.getToken();
        return userToken.length > 0 && userToken !== null;
    }

    public register(company: Register): Observable<Company> {
        return this.http.post<Company>(`${COMPANY_URL}/create`, company);
    }

    private saveUserDataToLocalStorage(user: User): void {
        if (user === null) return;
        localStorage.setItem('userData', JSON.stringify(user));
    }

    public getUserDataFromLocalStorage(): User | null {
        const rawUserData = localStorage.getItem('userData');

        return JSON.parse(rawUserData);
    }

    private removeUserDataFromLocalStorage(): void {
        localStorage.removeItem('userData');
    }
}
