import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Collection, CreateCollection } from 'src/app/types/collections/Collection';

import { environment } from '../../../environments/environment';
import { BehaviorSubject, Observable } from 'rxjs';
import { CollectionOrder } from 'src/app/types/collections/CollectionOrder';

const URL = environment.cmPaymentsCollectionsUrl;
const COLLECTION = URL + '/collection';

@Injectable({
  providedIn: 'root'
})
export class CollectionService {

  private collection$$ = new BehaviorSubject<Collection>(null);
  collection$ = this.collection$$.asObservable();

  constructor(private http: HttpClient) { }

  public createCollection(object: CreateCollection): Observable<CreateCollection> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
    });
    return this.http.post<CreateCollection>(COLLECTION, object,{headers});
  }

  public updateCollection(object: Collection): Observable<Collection> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
    });
    return this.http.put<Collection>(COLLECTION, object,{headers});
  }

  public setStateCollection(object: Collection): Observable<Collection> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
    });
    return this.http.put<Collection>(`${COLLECTION}/setstate`, object,{headers});
  }

  public getCollections(): Observable<Collection[]> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
    });
    return this.http.get<Collection[]>(COLLECTION,{headers});
  }

  public obtainCollections(groupInternalId: String): Observable<Collection[]> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
    });
    return this.http.get<Collection[]>(`${COLLECTION}/${groupInternalId}`,{headers});
  }

  public obtainCollectionsActiveAndInactive(groupInternalId: string): Observable<Collection[]> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
    });
    return this.http.get<Collection[]>(`${COLLECTION}/activeInactive/${groupInternalId}`,{headers});
  }

  public obtainByPeriodicityAndGroupInternalId(periodicity: string, groupInternalId: string): Observable<Collection[]> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
    });
    const params: HttpParams = new HttpParams()
      .set("periodiocity", periodicity)
      .set("groupInternalId", groupInternalId);
    return this.http.get<Collection[]>(`${COLLECTION}/CollectionByPeriodicityAndGroupInternalId`, { params, headers });
  }

  public obtainByStateAndGroupInternalId(state: string, groupInternalId: string): Observable<Collection[]> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
    });
    const params: HttpParams = new HttpParams()
      .set("state", state)
      .set("groupInternalId", groupInternalId);
    return this.http.get<Collection[]>(`${COLLECTION}/CollectionByStateAndGroupInternalId`, { params, headers })
  }

  public obtainServices(): Observable<Collection[]> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
    });
    return this.http.get<Collection[]>(`${COLLECTION}/services`);
  }

  public setCollection(collection: Collection) {
    this.collection$$.next(collection);
  }

}
