import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { CollectionOrder } from 'src/app/types/collections/CollectionOrder';

import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';

const URL = environment.cmPaymentsCollectionsUrl;
const COLLECTIONORDER = URL + '/collectionorder';

@Injectable({
  providedIn: 'root'
})
export class CollectionOrderService {

  constructor(private http: HttpClient) { }

  public createOne(collection: CollectionOrder): Observable<CollectionOrder> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
    });
    return this.http.post<CollectionOrder>(`${COLLECTIONORDER}/createOne`, collection, { headers });
  }

  public updateOne(collection: CollectionOrder): Observable<CollectionOrder> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
    });
    return this.http.put<CollectionOrder>(`${COLLECTIONORDER}/updateCollectionOrder`, collection, { headers });
  }

  public deleteById(internalId: string): Observable<any> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
    });
    return this.http.delete<any>(`${COLLECTIONORDER}/deleteCollectionOrder/${internalId}`, { headers });
  }

  public processByCollectionId(collectionId: string): Observable<any> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
    });
    return this.http.put<any>(`${COLLECTIONORDER}/processOrder/`, {}, {params: {
      collectionId: collectionId
    }, headers},);
  }

  public createCollectionOrder(object: CollectionOrder[]): void {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
    });
    this.http.post<CollectionOrder>(COLLECTIONORDER, object, {headers}).subscribe(
      response => { }, error => { }
    );
  }

  public setStateCollection(id: string[]): void {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
    });
    this.http.put<string[]>(`${COLLECTIONORDER}/collectedamount`, id, {headers}).subscribe(
      response => { }, error => { }
    );
  }

  public setReadyCollection(id: string[]): void {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
    });
    this.http.put<string[]>(`${COLLECTIONORDER}/readycollection`, id, {headers}).subscribe(
      response => { }, error => { }
    );
  }

  public uploadToPayCsv(collectionId: string, csv: File): Observable<any> {
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
    });
    const formData: FormData = new FormData();
    formData.append('file', csv);
    return this.http.post(`${COLLECTIONORDER}/uploadToPayCsv/${collectionId}`, formData, {headers: headers, responseType:"text"});
  }

  public validateCSVFile(file: File): Observable<CollectionOrder> {
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
    });
    const formData: FormData = new FormData();
    formData.append('file', file);
    return this.http.post<CollectionOrder>(`${COLLECTIONORDER}/validateCSVfile`, formData,  {headers});
  }

  public obtainByReferenceIdAndCollectionId(collectionId: string, referenceId: string): Observable<CollectionOrder[]> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
    });
    const params: HttpParams = new HttpParams()
      .set("collectionId", collectionId)
      .set("referenceId", referenceId);
    return this.http.get<CollectionOrder[]>(`${COLLECTIONORDER}/collectionOrderByReferenceIdAndCollectionId`, { params, headers })
  }

  public reportCollection(serviceName: string, state: string[], dateTo: Date, dateFrom: Date): Observable<CollectionOrder[]> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
    });
    const dateFromFormat = dateFrom.toISOString().split('T')[0];
    const dateToFormat = dateTo.toISOString().split('T')[0];
    return this.http.get<CollectionOrder[]>(`${COLLECTIONORDER}/report/${serviceName}/${state}/${dateToFormat}/${dateFromFormat}`, {headers});
  }

  public obtainByCollectionId(collectionId: string): Observable<CollectionOrder[]> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
    });
    return this.http.get<CollectionOrder[]>(`${COLLECTIONORDER}/${collectionId}`, {headers});
  }

  public payCollectionOrder(internalId: string, paymentWay: string): Observable<string>  {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
    });
    const params: HttpParams = new HttpParams()
      .set("internalId", internalId)
      .set("paymentWay", paymentWay);
      console.log(internalId)
    return this.http.put<string>(`${COLLECTIONORDER}/payCollectionOrder`, { headers, params });
  }

  public payOrderCollection(internalId: string): Observable<string> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
    });
    return this.http.put<string>(`${COLLECTIONORDER}/payCollectionOrder/${internalId}`, null, { headers });
  }

  public obtainByStateAndCollectionId(state: string, collectionId: string): Observable<CollectionOrder[]> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
    });
    const params: HttpParams = new HttpParams()
      .set("state", state)
      .set("collectionId", collectionId);
    return this.http.get<CollectionOrder[]>(`${COLLECTIONORDER}/CollectionOrderByStateAndCollectionId`, { params, headers });
  }

  public obtainByStateAndGroupInternalId(state: string, groupInternalId: string): Observable<CollectionOrder[]> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
    });
    const params: HttpParams = new HttpParams()
      .set("state", state)
      .set("groupInternalId", groupInternalId);
    return this.http.get<CollectionOrder[]>(`${COLLECTIONORDER}/CollectionByStateAndGroupInternalId`, {headers});
  }

  public obtainByCollectionServiceName(serviceName: string, referenceId: string): Observable<CollectionOrder[]> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
    });
    const params: HttpParams = new HttpParams()
      .set("serviceName", serviceName)
      .set("referenceId", referenceId);
    return this.http.get<CollectionOrder[]>(`${COLLECTIONORDER}//CollectionOrderByCollectionServiceName`, { params, headers });
  }

  public obtainByCustomerId(id: string): Observable<CollectionOrder[]> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
    });
    return this.http.get<CollectionOrder[]>(`${COLLECTIONORDER}/orderCustomer/${id}`,{headers});
  }

  public obtainOrdersByDebtorAccount(account: number): Observable<CollectionOrder[]> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
    });
    return this.http.get<CollectionOrder[]>(`${COLLECTIONORDER}/collectionOrderByDebtorAccount/${account}`,{headers});
  }

  public obtainOrdersByDebtorAccountAndPayWay(account: number, payway: string): Observable<CollectionOrder[]> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
    });
    return this.http.get<CollectionOrder[]>(`${COLLECTIONORDER}/collectionsPaymentWay/${account}/${payway}`,{headers});
  }

  public changePaymentWay(id: string): Observable<any> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
    });
    return this.http.put<any>(`${COLLECTIONORDER}/changePaymentWay/${id}`,null, {headers});
  }

}