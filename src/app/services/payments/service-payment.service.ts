import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from '../../../environments/environment';
import { ServiceOffered } from './../../types/payments/ServiceOffered';

const URL = environment.cmPaymentsCollectionsUrl;
const SERVICEPAYMENT = URL + '/servicePayment';

@Injectable({
  providedIn: 'root'
})
export class ServicePaymentService {

  constructor(private http: HttpClient) { }

  public create(object: ServiceOffered): void {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
    });
    this.http.post<ServiceOffered>(SERVICEPAYMENT, object, {headers}).subscribe(
      response => {}, error => {}
    );
  }

  public approve(id: string): void {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
    });
    this.http.put<string[]>(`${SERVICEPAYMENT}/${id}`, id, {headers}).subscribe(
      response => {}, error => {}
    );
  }

  public obtainServicesPaymentFromDateEndDate(initial: string, final: string): Observable<ServiceOffered[]> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
    });
    return this.http.get<ServiceOffered[]>(`${SERVICEPAYMENT}/${initial}/${final}`, {headers});
  }

}
