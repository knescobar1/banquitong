import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from '../../../environments/environment';
import { ServiceOffered } from './../../types/payments/ServiceOffered';

const URL = environment.cmPaymentsCollectionsUrl;
const SERVICEOFFERED = URL + '/serviceOffered';

@Injectable({
  providedIn: 'root'
})
export class ServiceOfferedService {

  constructor(private http: HttpClient) { }

  public create(object: ServiceOffered): void {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
    });
    this.http.post<ServiceOffered>(SERVICEOFFERED, object).subscribe(
      response => {}, error => {}
    );
  }

  public approve(id: string): Observable<ServiceOffered> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
    });
    return this.http.put<ServiceOffered>(`${SERVICEOFFERED}/${id}`, id, {headers});
  }

  public payServiceOffered(id: string, amount: number): Observable<ServiceOffered> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
    });
    const params: HttpParams = new HttpParams()
    .set("id", id)
    .set("amount", amount);
    return this.http.put<ServiceOffered>(`${SERVICEOFFERED}/pay/${id}/${amount}`, { params, headers });
  }

  public obtainAllServicesOffered(): Observable<ServiceOffered[]> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
    });
    return this.http.get<ServiceOffered[]>(`${SERVICEOFFERED}/all`, {headers});
  }

  public obtainPendingServicesOffered(groupId: string): Observable<ServiceOffered[]> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
    });
    return this.http.get<ServiceOffered[]>(`${SERVICEOFFERED}/pending/${groupId}`,{headers});
  }

  public obtainApprovedServicesOffered(): Observable<ServiceOffered[]> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
    });
    return this.http.get<ServiceOffered[]>(`${SERVICEOFFERED}/approved`,{headers});
  }

  public obtainInactiveRecurrenceServicesOffered(): Observable<ServiceOffered[]> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
    });
    return this.http.get<ServiceOffered[]>(`${SERVICEOFFERED}/inactive`,{headers});
  }

  public obtainAactiveRecurrenceServicesOffered(): Observable<ServiceOffered[]> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
    });
    return this.http.get<ServiceOffered[]>(`${SERVICEOFFERED}/active`,{headers});
  }

}
