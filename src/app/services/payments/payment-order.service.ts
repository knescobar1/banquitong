import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';

import { environment } from '../../../environments/environment';
import { PaymentOrder } from '../../types/payments/PaymentOrder';

const URL = environment.cmPaymentsCollectionsUrl;
const PAYMENTORDER = URL + '/paymentOrder';

@Injectable({
  providedIn: 'root'
})
export class PaymentOrderService {

  constructor(private http: HttpClient) { }

  allPaymentOrders: PaymentOrder[] = [];

  public appendOrder(paymentOrder: PaymentOrder): void {
    console.log(paymentOrder);
    this.allPaymentOrders.push(paymentOrder);
  }

  public create(object: PaymentOrder, id: string): Observable<PaymentOrder> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
    });
    return this.http.post<PaymentOrder>(`${PAYMENTORDER}/order/${id}`, object, {headers});
  }

  public createOrders(object: PaymentOrder[], id: string): Observable<PaymentOrder[]> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
    });
    return this.http.post<PaymentOrder[]>(`${PAYMENTORDER}/paymentorders/${id}`, object, {headers});
  }

  public createOrdersByCsv(file: File, id: string): Observable<PaymentOrder[]> {
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
    });
    const formData: FormData = new FormData();
    formData.append('file', file);
    return this.http.post<PaymentOrder[]>(`${PAYMENTORDER}/createOrdersByCSV/${id}`, formData, {headers});
  }

  public validateCSVFile(file: File): Observable<PaymentOrder> {
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
    });
    const formData: FormData = new FormData();
    formData.append('file', file);
    return this.http.post<PaymentOrder>(`${PAYMENTORDER}/validateCSVfile`, formData, {headers});
  }

  public approve(id: string): Observable<PaymentOrder> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
    });
    return this.http.put<PaymentOrder>(`${PAYMENTORDER}/${id}`, id, {headers});
  }

  public deleteOrder(id: string): Observable<PaymentOrder> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
    });
    return this.http.put<PaymentOrder>(`${PAYMENTORDER}/delete/${id}`, id, {headers});
  }

  public obtainByPaymentId(id: string): Observable<PaymentOrder> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
    });
    return this.http.get<PaymentOrder>(`${PAYMENTORDER}/${id}`, {headers});
  }

  public obtainOrdersByProperties(groupId: string, state: string, id?: string, fromDate?: Date, endDate?: Date): Observable<PaymentOrder[]> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
    });
    const params: HttpParams = new HttpParams()
      .set("groupId", groupId)
      .set("id", id)
      .set("state", state)
      .set("fromDate", fromDate?.toISOString() ?? '')
      .set("endDate", endDate?.toISOString() ?? '');

    return this.http.get<PaymentOrder[]>(PAYMENTORDER, { params, headers });
  }

}
