import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from '../../../environments/environment';
import { Payment } from '../../types/payments/Payment';

const URL = environment.cmPaymentsCollectionsUrl;
const PAYMENT = URL + '/payment';

@Injectable({ providedIn: 'root' })
export class PaymentService {

  constructor(private http: HttpClient) { }

  public create(object: Payment): Observable<Payment> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
    });
    return this.http.post<Payment>(PAYMENT, object, {headers});
  }

  public approve(id: string, mail: string): Observable<Payment> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
    });
    return this.http.put<Payment>(`${PAYMENT}/${id}/${mail}`, id, {headers});
  }

  public obtainPaymentsByState(state: string, groupId: string, serviceName?: string, fromDate?: Date, endDate?: Date): Observable<Payment[]> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
    });
    const params: HttpParams = new HttpParams()
      .set("state", state)
      .set("groupId", groupId)
      .set("fromDate", fromDate?.toISOString() ?? '')
      .set("endDate", endDate?.toISOString() ?? '')
      .set("serviceName", serviceName);

    return this.http.get<Payment[]>(`${PAYMENT}/state`, { params, headers });
  }

  public obtainPaymentsByProperties(groupId: string, id: string, serviceName: string, state: string, fromDate: Date, endDate: Date, description: string): Observable<Payment[]> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
    });
    const params: HttpParams = new HttpParams()
      .set("groupId", groupId)
      .set("id", id)
      .set("serviceName", serviceName ?? '')
      .set("state", state === 'APROBADO' ? 'APP' : 'PEN')
      .set("fromDate", fromDate?.toISOString() ?? '')
      .set("endDate", endDate?.toISOString() ?? '')
      .set("description", description ?? '');

    return this.http.get<Payment[]>(PAYMENT, { params, headers });
  }


}
