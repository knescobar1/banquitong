import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { GeneralLedgerReport } from 'src/app/types/accounting/GeneralLedgerReport';
import { JournalEntry } from 'src/app/types/accounting/JournalEntry';
import { Period } from 'src/app/types/accounting/Period';
import { Transaction } from 'src/app/types/payments/Transaction';
import { environment } from 'src/environments/environment';

const URL = environment.cmAccountingUrl;
const JOURNALENTRY = URL + '/JournalEntry';

@Injectable({
  providedIn: 'root'
})
export class JournalEntryService {

  constructor(private http: HttpClient) { }

  public getJournalEntries(): Observable<JournalEntry[]> {
    return this.http.get<JournalEntry[]>(`${JOURNALENTRY}`);
  }

  public getReportByPeriod(from: Date, to: Date): Observable<GeneralLedgerReport[]> {
    const dateFrom = from.toISOString().split('T')[0];
    const dateTo = to.toISOString().split('T')[0];
    return this.http.get<GeneralLedgerReport[]>(`${JOURNALENTRY}/Report/${dateFrom}/${dateTo}`);
  }

  public getJournalEntryInAccountingPeriod(period: Period): Observable<JournalEntry[]> {
    return this.http.get<JournalEntry[]>(`${JOURNALENTRY}/AccountingPeriod/${period.dateFrom}/${period.dateTo}`);
  }

  public getJournalEntryByTransactionReference(reference: string): Observable<JournalEntry> {
    return this.http.get<JournalEntry>(`${JOURNALENTRY}/TransactionReference`, {
      params: {
        transactionReference: reference
      }
    });
  }

  public getJournalEntryByGeneralLedgerAccount(account: string): Observable<JournalEntry[]> {
    return this.http.get<JournalEntry[]>(`${JOURNALENTRY}/GeneralLedgerAccount`, {
      params: {
        generalLedgerAcount: account
      }
    });
  }

  public create(transaction: Transaction): Observable<any> {
    return this.http.post<any>(`${JOURNALENTRY}`, transaction);
  }
}
