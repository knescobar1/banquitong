import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { GeneralLedgerAccount } from 'src/app/types/accounting/GeneralLedgerAccount';
import { environment } from 'src/environments/environment';

const URL = environment.cmAccountingUrl;
const GENERALLEDGERACCOUNT = URL + '/GeneralLedgerAccount';

@Injectable({
  providedIn: 'root'
})
export class GeneralLedgerAccountService {

  constructor(private http: HttpClient) { }

  public getLedgerAccountByCode(code: string): Observable<GeneralLedgerAccount> {
    return this.http.get<GeneralLedgerAccount>(`${GENERALLEDGERACCOUNT}/ledgerAccount/code/${code}`);
  }

  public getLedgerAccountByType(type: string): Observable<GeneralLedgerAccount> {
    return this.http.get<GeneralLedgerAccount>(`${GENERALLEDGERACCOUNT}/ledgerAccount/type/${type}`);
  }

  public create(generalLedger: GeneralLedgerAccount): Observable<GeneralLedgerAccount> {
    return this.http.post<GeneralLedgerAccount>(`${GENERALLEDGERACCOUNT}`, generalLedger);
  }

  public update(generalLedger: GeneralLedgerAccount): Observable<GeneralLedgerAccount> {
    return this.http.put<GeneralLedgerAccount>(`${GENERALLEDGERACCOUNT}`, generalLedger);
  }
}
