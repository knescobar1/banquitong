import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { MainLayoutComponent } from './layouts/app.main.component';

import { HomeComponent } from './pages/home/home.component';
import { ConsolidatedComponent } from './pages/consolidated/consolidated.component';
import { LoginComponent } from './pages/login/login.component';
import { AccountsComponent } from './pages/gestion-empresarial/accounts/accounts.component';
import { NewUserComponent } from './pages/gestion-empresarial/newuser/new-user.component';

import { CollectionsConfigComponent } from './pages/collections/collections-config/collections-config.component';
import { CollectionsEnableComponent } from './pages/collections/collections-enable/collections-enable.component';
import { CollectionsProcessComponent } from './pages/collections/collections-process/collections-process.component';
import { CollectionsReportsComponent } from './pages/collections/collections-reports/collections-reports.component';
import { CollectionsListComponent } from './pages/collections/collections-list/collections-list.component';
import { CollectionsNeworderComponent } from './pages/collections/collections-neworder/collections-neworder.component';
import { AuthGuardService } from './services/utils/auth-guard.service';
import { RegisterComponent } from './pages/register/register.component';
import { CollectionsReferenceidComponent } from './pages/collections/collections-referenceid/collections-referenceid.component';

import { AccountingBookConfigComponent } from './pages/accounting/accounting-book-config/accounting-book-config.component';
import { AccountingBookReportComponent } from './pages/accounting/accounting-book-report/accounting-book-report.component';

import { PaymentsTransactionsReportComponent } from './pages/payments/payments-transactions-report/payments-transactions-report.component';
import { PaymentsOrdersReportComponent } from './pages/payments/payments-orders-report/payments-orders-report.component';
import { PaymentsAproveComponent } from './pages/payments/payments-aprove/payments-aprove.component';
import { PaymentsRecurrentRegisteredComponent } from './pages/payments/payments-recurrent-registered/payments-recurrent-registered.component';
import { PaymentsRecurrentConfigComponent } from './pages/payments/payments-recurrent-config/payments-recurrent-config.component';
import { PaymentrefLoadorderComponent } from './pages/payments/payments-referenceid/paymentref-loadorder/paymentref-loadorder.component';
import { PaymentBasicAddComponent } from './pages/payments/payments-basic-services/payment-basic-add/payment-basic-add.component';
import { PaymentBasicOrdersComponent } from './pages/payments/payments-basic-services/payment-basic-orders/payment-basic-orders.component';
import { UsersComponent } from './pages/gestion-empresarial/users/users.component';
import { RoleGuard } from './guards/role.guard';
import { PayServiceComponent } from './pages/customer-services/pay-service/pay-service.component';
import { ConfigRecurrentPaymentsComponent } from './pages/customer-services/config-recurrent-payments/config-recurrent-payments.component';
import { PaymentsHistoryComponent } from './pages/customer-services/payments-history/payments-history.component';
import { DeactivateRecurrentPaymentsComponent } from './pages/customer-services/deactivate-recurrent-payments/deactivate-recurrent-payments.component';

const routes: Routes = [
  {
    path: '',
    component: MainLayoutComponent,
    canActivate: [AuthGuardService],
    children: [
      { path: '', component: HomeComponent, canActivate: [AuthGuardService] },
      {
        path: 'pay-service',
        component: PayServiceComponent,
        canActivate: [AuthGuardService, RoleGuard],
        canLoad: [RoleGuard],
      },
      {
        path: 'config-recurrent-payments',
        component: ConfigRecurrentPaymentsComponent,
        canActivate: [AuthGuardService, RoleGuard],
        canLoad: [RoleGuard],
      },
      {
        path: 'deactivate-recurrent-payments',
        component: DeactivateRecurrentPaymentsComponent,
        canActivate: [AuthGuardService, RoleGuard],
        canLoad: [RoleGuard],
      },
      {
        path: 'payments-history',
        component: PaymentsHistoryComponent,
        canActivate: [AuthGuardService, RoleGuard],
        canLoad: [RoleGuard],
      },
      {
        path: 'consolidated',
        component: ConsolidatedComponent,
        canActivate: [AuthGuardService, RoleGuard],
        canLoad: [RoleGuard],
      },
      {
        path: 'users',
        component: UsersComponent,
        canActivate: [AuthGuardService, RoleGuard],
        canLoad: [RoleGuard],
      },
      {
        path: 'accounts',
        component: AccountsComponent,
        canActivate: [AuthGuardService, RoleGuard],
        canLoad: [RoleGuard],
      },
      {
        path: 'newuser',
        component: NewUserComponent,
        canActivate: [AuthGuardService, RoleGuard],
        canLoad: [RoleGuard],
      },
      {
        path: 'collections/config',
        component: CollectionsConfigComponent,
        canActivate: [AuthGuardService, RoleGuard],
        canLoad: [RoleGuard],
      },
      {
        path: 'collections/enable',
        component: CollectionsEnableComponent,
        canActivate: [AuthGuardService, RoleGuard],
        canLoad: [RoleGuard],
      },
      {
        path: 'collections/list',
        component: CollectionsListComponent,
        canActivate: [AuthGuardService, RoleGuard],
        canLoad: [RoleGuard],
      },
      {
        path: 'collections/process',
        component: CollectionsProcessComponent,
        canActivate: [AuthGuardService],
      },
      {
        path: 'collections/referenceid',
        component: CollectionsReferenceidComponent,
        canActivate: [AuthGuardService, RoleGuard],
        canLoad: [RoleGuard],
      },
      {
        path: 'collections/reports',
        component: CollectionsReportsComponent,
        canActivate: [AuthGuardService],
      },
      {
        path: 'collections/neworder',
        component: CollectionsNeworderComponent,
        canActivate: [AuthGuardService, RoleGuard],
        canLoad: [RoleGuard],
      },
      {
        path: 'payments/loadorder',
        component: PaymentrefLoadorderComponent,
        canActivate: [AuthGuardService, RoleGuard],
        canLoad: [RoleGuard],
      },
      {
        path: 'payments/transactionsreport',
        component: PaymentsTransactionsReportComponent,
        canActivate: [AuthGuardService, RoleGuard],
        canLoad: [RoleGuard],
      },
      {
        path: 'payments/recurrentregister',
        component: PaymentsRecurrentRegisteredComponent,
        canActivate: [AuthGuardService, RoleGuard],
        canLoad: [RoleGuard],
      },
      {
        path: 'payments/recurrentconfig',
        component: PaymentsRecurrentConfigComponent,
        canActivate: [AuthGuardService, RoleGuard],
        canLoad: [RoleGuard],
      },
      {
        path: 'payments/loadorder',
        component: PaymentrefLoadorderComponent,
        canActivate: [AuthGuardService],
      },
      {
        path: 'payments/ordersreport',
        component: PaymentsOrdersReportComponent,
        canActivate: [AuthGuardService, RoleGuard],
        canLoad: [RoleGuard],
      },
      {
        path: 'payments/aprove',
        component: PaymentsAproveComponent,
        canActivate: [AuthGuardService],
      },
      {
        path: 'payments/basicregister',
        component: PaymentBasicAddComponent,
        canActivate: [AuthGuardService, RoleGuard],
        canLoad: [RoleGuard],
      },
      {
        path: 'payments/basicorders',
        component: PaymentBasicOrdersComponent,
        canActivate: [AuthGuardService],
      },

      {
        path: 'accounting/config',
        component: AccountingBookConfigComponent,
        canActivate: [AuthGuardService, RoleGuard],
        canLoad: [RoleGuard],
      },
      {
        path: 'accounting/report',
        component: AccountingBookReportComponent,
        canActivate: [AuthGuardService, RoleGuard],
        canLoad: [RoleGuard],
      },
    ],
  },
  { path: 'pages/login', component: LoginComponent },
  { path: 'pages/register', component: RegisterComponent },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      scrollPositionRestoration: 'enabled',
      anchorScrolling: 'enabled',
      relativeLinkResolution: 'corrected',
    }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
