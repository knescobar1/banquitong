export const environment = {
  production: false,
  // UrlService: 'http://34.171.121.24:8080/',
  // URLs APIGATEWAY - KONG
  cmAccountingUrl: 'http://34.71.130.180:8080',
  cmClientsUrl: 'http://35.209.18.159:8000/cmclients',
  cmPaymentsCollectionsUrl: 'http://35.209.18.159:8000/cmpaymentscollections',
  coreAccounntsUrl: 'http://35.209.18.159:8000/coreaccounts',
  coreCustomersUrl: 'http://35.209.18.159:8000/corecustomers',
  coreSettingsUrl: 'http://35.209.18.159:8000/coresettings',
};
